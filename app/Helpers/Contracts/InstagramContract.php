<?php

namespace App\Helpers\Contracts;

Interface InstagramContract
{

    public function fetch();

    public function getReady();

    public function test();
}