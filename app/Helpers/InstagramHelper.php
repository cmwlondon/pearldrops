<?php

namespace app\Helpers;

use App\Helpers\Contracts\InstagramContract;

use App\Models\Instagram as InstagramModel;

use GuzzleHttp\Client;
use Tinify;
use Illuminate\Support\Facades\Storage;
use Vinkla\Instagram\Instagram;

use Illuminate\Support\Facades\Cache;

class InstagramHelper implements InstagramContract
{
    public function getReady() {
   		/*
   		$cache_id = 'pd_instagram';
		if (Cache::has($cache_id)) {
			return cache($cache_id);
		} else {
    		$ready = InstagramModel::where('state', 'ready')->limit(20)->get();
    		cache([$cache_id => $ready], 30);
    		return $ready;
    	}
    	*/
    	return InstagramModel::where('state', 'ready')->latest()->limit(20)->get();
    }


    // https://www.instagram.com/pearldropsuk/

    public function fetch()
    {	
    	// vinkla/instagram endpoint closed 29/7/2020

		// Get most recent Image from the database
		// use this as cutoff for items in instagram timeline
		$latest = InstagramModel::orderBy('created_time', 'desc')
           ->take(1)
           ->get()
           ->first();

        if ($latest == null) {
        	$last_time = 0;
        } else {
        	$last_time = $latest->created_time;
        }

	   	// get instagram posts for appropriate user account 'femfresh_uk' (https://www.instagram.com/femfresh_uk/) in JSON format
	   	// needless to say, if Instagram change their access policies or JSON format at some point in the future this will need to be updated
	   	$response = file_get_contents('https://www.instagram.com/pearldropsuk/');

	   	preg_match('/<script type="text\/javascript">window[.]_sharedData = (\{[\s\S]*\});<\/script>/', $response, $matches);
		$media = json_decode($matches[1]); 
		$userdata = $media->entry_data->ProfilePage[0]->graphql->user;

		if( count($userdata->edge_owner_to_timeline_media->edges) > 0 )
		{
			foreach( $userdata->edge_owner_to_timeline_media->edges AS $post) {
				// if post is newer than latest post in db, insert into db
	   			if ($last_time < $post->node->taken_at_timestamp) {

		   			$image = new InstagramModel;
		   			$image->id_str = $post->node->id.'_5720822920'; // [item id]_[user id]
		   			$image->code = ''; // $item->code;
		   			$image->media_remote = $post->node->thumbnail_src;
		   			$image->media_local = '';
		   			$image->black = false;
		   			$image->state = 'pending';
		   			$image->link = 'https://www.instagram.com/p/'.$post->node->shortcode;
		   			$image->type = 'image'; // not sure where to find the post type in the JSON format so default to 'image' for the moment
		   			$image->created_time = $post->node->taken_at_timestamp;

		   			$image->save();
	   			}
			}

		}

		// process FIRST 'pending' item in 'instagrams' table
		return $this->process();
	}

    public function test()
    {	
	}

    private function process()
    {
    	$server_path = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
    	$pending = InstagramModel::where('state', 'pending')->get();

    	if ( count($pending) > 0 ) {
	    	foreach($pending as $image) {
	    		if ($image->type == "video") {
	   				$image->black = $this->check_if_black($image->media_remote);
	   			}

	   			$image->media_local = $image->id_str . ".jpg";

	   			$source = Tinify::fromUrl($image->media_remote);
				$source->resize(["method" => "cover", "width" => 320, "height" => 320])->toFile($server_path . 'instagram/small_' . $image->media_local);
				$source->resize(["method" => "cover", "width" => 480, "height" => 480])->toFile($server_path . 'instagram/medium_' . $image->media_local);
				$source->resize(["method" => "cover", "width" => 640, "height" => 640])->toFile($server_path . 'instagram/large_' . $image->media_local);

				$image->state = 'ready';
				$image->save();
				return $image->id_str.' / '.$image->media_remote;
	    	}
    	} else {
    		return 'none';
    	}
	}		    

	/*
    public function fetch()
    {	
    	$access_token = env('INSTAGRAM_TOKEN', '');
    	$instagram = new Instagram($access_token);

		//Get most recent Image from the database
		$latest = InstagramModel::orderBy('created_time', 'desc')
           ->take(1)
           ->get()
           ->first();

        if ($latest == null) {
        	$last_time = 0;
        } else {
        	$last_time = $latest->created_time;
        }

	   	$media = $instagram->media();

	   	if (count($media) > 0) {
	   		foreach($media as $item) {
	   			// Only process if newer than our most recent image.
	   			if ($last_time < $item->created_time) {
		   			
		   			$image = new InstagramModel;
		   			$image->id_str = $item->id;
		   			$image->code = '';//$item->code;
		   			$image->media_remote = $item->images->standard_resolution->url;
		   			$image->media_local = '';
		   			$image->black = false;
		   			$image->state = 'pending';
		   			$image->link = $item->link;
		   			$image->type = $item->type;
		   			$image->created_time = $item->created_time;

		   			$image->save();
		   		}
	   		}
	   	}
		
		$this->process();

		// added to get CLI command 'php artisan instagram:check' to work without generating an error due to null string returned
		return 'instagram:check complete items: '.count($media);
    }

    public function test()
    {	
    	$access_token = env('INSTAGRAM_TOKEN', '');
    	$instagram = new Instagram($access_token);
	   	$media = $instagram->media();

    	return $media;
	}

    private function process() {
    	$server_path = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();

    	$pending = InstagramModel::where('state', 'pending')->get();
    	foreach($pending as $image) {
    		if ($image->type == "video") {
   				$image->black = $this->check_if_black($image->media_remote);
   			}

   			$image->media_local = $image->id_str . ".jpg";

   			$source = Tinify::fromUrl($image->media_remote);
			$source->resize(["method" => "cover", "width" => 320, "height" => 320])->toFile($server_path . 'instagram/small_' . $image->media_local);
			$source->resize(["method" => "cover", "width" => 480, "height" => 480])->toFile($server_path . 'instagram/medium_' . $image->media_local);
			$source->resize(["method" => "cover", "width" => 640, "height" => 640])->toFile($server_path . 'instagram/large_' . $image->media_local);

			$image->state = 'ready';
			$image->save();
			return false;
    	}
    	
    }
    */

    private function check_if_black($src){

	    $img = imagecreatefromjpeg($src);
	    list($width_orig, $height_orig)=getimagesize($src);
	    for($i=0;$i<20;$i++){
	        $rand_width=rand ( 0 , $width_orig-1 );
	        $rand_height=rand ( 0 , $height_orig-1 );
	        $rgb = imagecolorat($img, $rand_width, $rand_height);
	        if($rgb!=0){
	            return false;
	        }
	    }
	    return true;
	}   
}
