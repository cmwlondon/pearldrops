<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Helpers\InstagramHelper;

class InstagramServiceProvider extends ServiceProvider
{
    
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Helpers\Contracts\InstagramContract', function(){

            return new InstagramHelper();

        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['App\Helpers\Contracts\InstagramContract'];
    }
}
