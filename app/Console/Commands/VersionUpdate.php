<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class VersionUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'version:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates the version for the cache buster';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $config = config('context');
        $config['version']++;
        $this->line("Updating version to be: " . $config['version']);
        $encoded = str_replace(["{","}",":"],["[","]","=>"],stripslashes(json_encode($config)));
        $output = "<?php return " . $encoded . ";";
        Storage::disk('config')->put('context.php', $output);
    }
}
