<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class ErrorCreator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'errors:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates the cached versions of error pages such as the 404';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->context = config('context');
        $this->context['site_url'] = url('',[]);
        $this->context['pageViewCSS']   = ['/css/main/styles.css'];
        $this->context['meta']['fbid'] = env('FACEBOOK_APP_ID', '');
        $this->context['meta']['title'] = 'femfresh page not found';

        $this->context['articles'] = config('articles');
        $this->context['cookie'] = isset($_COOKIE['FemfreshDisclaimerCookie']);

        $this->context['canonical'] = url('404',[]);

        $rendered = view('main.gen_errors.404', $this->context)->render();
        $file = Storage::disk('errors')->put('404.blade.php', $rendered);
    }
}
