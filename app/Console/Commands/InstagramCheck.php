<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\Contracts\InstagramContract;

class InstagramCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'instagram:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks instagram for new posts.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(InstagramContract $instagram)
    {
        $output = $instagram->fetch();
        // $output is equal to NULL, should be a string, problem with the fetch method

        // $output = 'instagrame check';
        $this->line($output);
    }
}
