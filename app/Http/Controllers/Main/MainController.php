<?php namespace App\Http\Controllers\Main;

use App;
use Session;
use Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class MainController extends Controller {

	public function __construct() {
		$this->uri = Request::path();
		// Context now pulled from config so we can use it in error pages.
		$this->context = config('context');
		$this->context['site_url'] = url('',[]);
		$this->context['jsVars']['site_url'] = url('',[]);
		$this->context['meta']['link'] = url($this->uri);
		$this->context['meta']['image'] = url('');
		$this->context['meta']['fbid'] = env('FACEBOOK_APP_ID', '');
		$this->context['pageViewCSS']	= ['/css/main/styles.css'];//,'/css/main/print.css'
		$this->context['pageViewJS']	= '';

		$this->context['image_path'] = '/img';
		$this->context['articles'] = config('articles');
		$this->context['meta']['title'] = 'PAGE TITLE';
		$this->context['meta']['desc'] = 'PAGE DESCRIPTION';
		$this->context['meta']['keywords'] = 'PAGE KEYWORDS';
		$this->context['meta']['keyphrases'] = 'PAGE KEYPHRASES';
		$this->context['meta']['pagetype'] = 'article';
		$this->context['version'] = '1.0';
		$this->context['section'] = 'alpha';

		if (Session('response') !== NULL) {
			$this->context['response'] = Session('response');
		}

		// Check for meta in the config
		$meta = config('meta');
		if (array_key_exists($this->uri,$meta)) {
			foreach($this->context['meta'] as $key => $value) {
				if (array_key_exists($key,$meta[$this->uri])) {
					if ($key == "title" && $meta[$this->uri][$key] == "") {
						// Ignore blank titles;
					} elseif ($key == "image" && $meta[$this->uri][$key] != "") {
						$this->context['meta']['image'] = url('img/sharing/'.$meta[$this->uri][$key]);
					} else {
						$this->context['meta'][$key] = $meta[$this->uri][$key];
					}
				}
			}
		}

	}

	public function checkCache($preview) {
		// This messes up the cookie notice at the moment, so we could look at re-factoring this if loss of caching causes much of a problem.
		// if (App::environment('production')) {
		// 	if ($preview != true) {
		// 		if (Cache::has($this->uri)) {
		// 			return Cache::get($this->uri);
		// 		}
		// 	}
		// }
		return false;
	}

	public function saveCache($html) {
		if (App::environment('production','staging','testing')) {
			Cache::add($this->uri, $html, 60);
		}
	}

	public function forgetCache() {
		if (Cache::has($this->uri)) {
			Cache::forget($this->uri);
		}
	}

	public function clearCache() {
		Cache::flush();
	}
}
