<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;
use App\Services\SitemapPackager;

class MiscController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Show the application sitemap screen to the user.
	 *
	 * @return Response
	 */
	public function sitemap(SitemapPackager $packager)
	{	
		$pkg_response = $packager->create(); // Update the Sitemap		
		return $pkg_response;
	}

	/**
	 * Flush the cache
	 *
	 * @return Response
	 */
	public function flush()
	{	
		return $this->clearCache();
	}
}
