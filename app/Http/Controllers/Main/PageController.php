<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;
use Illuminate\Http\Request;
use Cookie;

use App\Http\Requests\Main\SupportRequest;
use App\Models\Support;
// use Illuminate\Support\Facades\Mail;
// use App\Mail\SysMail;
// use App\Mail\Enquiry;
use App;
use Mail;

use App\Helpers\Contracts\InstagramContract;

class PageController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(InstagramContract $instagram)
	{
		parent::__construct();
		$this->context['cookie'] = isset($_COOKIE['PearldropsDisclaimerCookie']);


		$this->context['instagram'] = $instagram->getReady();
		$this->context['jsVars']['instagram_bank'] = count($this->context['instagram']);
	}

	public function page(Request $request)
	{
		$url = $request->segment(1);
		$url = ($url == "") ? "home" : $url;
		// $this->context['pageViewJS']	= 'pages/pages.min';
		
		return $this->show($url, $request);
	}

	public function sub($product, Request $request)
	{
		$this->context['meta']['noIndex'] = true;
		$this->context['product'] = $product;
		return $this->show('our-collection-sub', $request);
	}

	public function misc(Request $request)
	{
		$url = $request->segment(1);
		$url = ($url == "") ? "home" : $url;
		return $this->show($url, $request);
	}

	public function contact(Request $request)
	{
		$this->context['titlesArr'] = ['' => 'Select', 'Mr' => 'Mr', 'Mrs' => 'Mrs', 'Miss' => 'Miss', 'Ms' => 'Ms'];

		$url = 'contact-us';
		return $this->show($url, $request);
	}

	public function contact_submit(SupportRequest $request, Support $support)
	{
		$support->fill($request->all());
		$support->save();

		// Prepare the email to send.
		$details = array(
			'title'			=> $request->title,
			'firstname'		=> $request->firstname,
			'lastname'		=> $request->lastname,
			'company'		=> '',//$request->company,
			'address'		=> '',//$request->address,
			'city'			=> '',//$request->city,
			'county'		=> '',//$request->county,
			'postcoe'			=> '',//$request->postcode,
			'country'		=> '',//$request->country,
			'phone'			=> $request->phone,
			'email'			=> $request->email,
			'product'		=> $request->product,
			'comments'		=> $request->comments
		);

		// if (App::environment() != 'local') {
			Mail::send(['text' => 'emails.enquiry'], $details, function($message) use($details)
			{
				$message->from( env('MAIL_NOREPLY'), env('MAIL_NAME') );
				$message->to( env('MAIL_SYS_TO') );
			    $message->subject( 'UK ChurchDwight Enquiry' );
			});
			
			// $mail = Mail::to(Config('mail.mail-admin-to'));
			
			// if (Config('mail.mail-admin-cc') != "") {
			// 	$mail->cc(Config('mail.mail-admin-cc'));
			// }
			// if (Config('mail.mail-admin-bcc') != "") {
			// 	$mail->bcc(Config('mail.mail-admin-bcc'));
			// }
			// $mail->send(new Enquiry($details));
		// }

		$this->response = array('response_status' => 'success', 'message' => 'Thank you for contacting Pearl Drops');

		return redirect()->route('contact')->with('response', $this->response);
	}

	public function mailtest(Request $request)
	{
		$this->context['time'] = date("Y-m-d H:i:s");

		$details = array(
			'title'			=> 'Mr',
			'firstname'		=> 'Albert',
			'lastname'		=> 'Einstein',
			'phone'			=> '7542087301',
			'email'			=> 'albert.einstain@gmail.com',
			'product'		=> 'replens mailgun test product',
			'comments'		=> 'replens meilgun test comment',
			'mfgcode'		=> 'pearldrops',
			'timestamp' 	=> date("Y-m-d H:i:s")
		);

		Mail::send(['text' => 'emails.enquiry'], $details, function($message) use($details)
		{
			$message->from( env('MAIL_NOREPLY'), env('MAIL_NAME') );
			$message->to( env('MAIL_SYS_TO') );
		    $message->subject( 'UK ChurchDwight Enquiry' );
		});

		return $this->show('mailtest', $request);
	}

	public function instacheck(Request $request, InstagramContract $instagram)
	{
    	$this->context['data'] = $instagram->test();
		$this->context['alpha'] = 'alpha';

		return $this->show('instacheck', $request);
	}

	private function show($url, $request)
	{
		if ($request->forget) {
			$this->forgetCache();
		}

		$cache = $this->checkCache($request->preview);
		if (!$cache) {
			
			if ($request->preview) {
				$this->context['nocache'] = true;
			}

			$cache = view('main.'.$url, $this->context)->render();

			if (!$request->preview) {
				$this->saveCache($cache);
			} else {
				$this->forgetCache();
			}
		}
		return $cache;
	}
}
