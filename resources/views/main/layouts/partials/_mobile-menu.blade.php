<ul class="mobile-menu">
  <div id="navigation" class="unav">
        <a href="{{ route('home') }}"><img src="/img/pearl-drops-logo.png" alt="pearldrops logo" class="mobile-nav-logo"></a>
        <img src="/img/hamburger.png" class="toggle-btn show-menu" alt="Open Menu">
        <img src="/img/close-icon.png" class="toggle-btn close-menu" alt="Close Menu">
        <nav class="unav-sidebar-show">
            <div class="gray-blue-mobile-link unav-item unav-dropdown">
                <a href="#">About us</a>
                <div class="gray-blue-mobile-link unav-dropdown-menu">
                    <div class="indented-link"><a href="{{ route('vision') }}" class="gray-blue-mobile-link sub-item">Our Vision</a></div>
                    <div class="indented-link"><a href="{{ route('history') }}" class="gray-blue-mobile-link sub-item">Our History</a></div>
                </div>
            </div>
            <a href="{{ route('science') }}" class="green-mobile-link unav-item">Our science</a>
            <div class="dark-blue-mobile-link unav-item unav-dropdown">
                <a href="#">Collection</a>
                <div class="dark-blue-mobile-link unav-dropdown-menu">
                    <div class="indented-link"><a href="/our-collection#lasting-flawless-white" class="btn-anchor dark-blue-mobile-link">Daily Whitening Toothpastes</a></div>
                    <div class="indented-link"><a href="/our-collection#lasting-flawless-white" class="btn-anchor dark-blue-mobile-link collection-link sub-item">Lasting Flawless White</a></div>
                    <div class="indented-link"><a href="/our-collection#instant-natural-white" class="btn-anchor dark-blue-mobile-link sub-item">Instant Natural White</a></div>
                    <div class="indented-link"><a href="/our-collection#luminous-bright-white" class="btn-anchor dark-blue-mobile-link sub-item">Luminous Bright White</a></div>
                    <div class="indented-link"><a href="/our-collection#strong-polished-white" class="btn-anchor dark-blue-mobile-link sub-item">Strong Polished White</a></div>
                    {{-- <div class="indented-link"><a href="/our-collection#pure-natural-white" class="btn-anchor dark-blue-mobile-link sub-item">Pure Natural White</a></div> --}}
                    
                    <div class="indented-link"><a href="/our-collection#professional-whitening-treatment" class="btn-anchor dark-blue-mobile-link">Whitening Treatments</a></div>
                    <div class="indented-link"><a href="/our-collection#professional-whitening-treatment" class="btn-anchor dark-blue-mobile-link sub-item">Professional Whitening Treatment</a></div>
                    <div class="indented-link"><a href="/our-collection#strong-and-white" class="btn-anchor dark-blue-mobile-link sub-item">Overnight Whitening Serum</a></div>
                    
                </div>
            </div>
            {{-- <a href="{{ route('contact') }}" class="pink-mobile-link unav-item">Contact us</a> --}}
            <a href="{{ route('buy') }}" class="gray-mobile-link unav-item">Where to buy</a>
        </nav>
    </div>
</ul>