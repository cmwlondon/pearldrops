<nav class="navbar navbar-expand-lg navbar-desktop bg-primary hidden-md-down">
    <div id="nav-container" class="container">
        <a href="{{ route('home') }}"><img class="brand-logo" src="/img/pearl-drops-logo.png" href="#"></a>
        <ul id="nav-main" class="navbar-nav ml-auto">
            <div class="dropdown">
                <a href="{{ route('vision') }}" data-toggle="dropdown" id="dropdownMenuButton" aria-haspopup="true" aria-expanded="false">About us</a>
                <li class="nav-item">
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <div class="container dd-left-align">
                            <div class="dropdown-links-container clearfix">
                                <div class="dropdown-pearldrops-links">
                                    <ul>
                                        <li><a href="{{ route('vision') }}" class="title-link">Our Vision</a></li>
                                    </ul>
                                </div>
                                <div class="dropdown-pearldrops-links">
                                    <ul>
                                        <li><a href="{{ route('history') }}" class="title-link">Our History</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </div>
            <li class="navbar-item">
                <a class="nav-link" href="{{ route('science') }}">Our Science</a>
            </li>
            <div class="dropdown">
                <a href="{{ route('collection') }}" id="dropdownMenuButton" aria-haspopup="true" aria-expanded="false">Collection</a>
                <li class="nav-item">
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <div class="container dd-left-align">
                           <div class="dropdown-links-container clearfix">
                                <div class="dropdown-pearldrops-links">
                                    <ul>
                                        <li><a href="/our-collection#lasting-flawless-white" class="title-link btn-anchor">Daily Whitening Toothpastes</a></li>
                                        <li><a href="/our-collection#lasting-flawless-white" class="btn-anchor">Lasting Flawless White</a></li>
                                        <li><a href="/our-collection#instant-natural-white" class="btn-anchor">Instant Natural White</a></li>
                                        <li><a href="/our-collection#luminous-bright-white" class="btn-anchor">Luminous Bright White</a></li>
                                        <li><a href="/our-collection#strong-polished-white" class="btn-anchor">Strong Polished White</a></li>
                                        {{-- <li><a href="/our-collection#pure-natural-white" class="btn-anchor">Pure Natural White</a></li> --}}
                                    </ul>
                                </div>
                                <div class="dropdown-pearldrops-links">
                                    <ul>
                                        <li><a href="/our-collection#professional-whitening-treatment" class="title-link btn-anchor">Whitening Treatments</a></li>
                                        <li><a href="/our-collection#professional-whitening-treatment" class="btn-anchor">Professional Whitening Treatment</a></li>
                                        <li><a href="/our-collection#strong-and-white" class="btn-anchor">Overnight Whitening Serum</a></li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </div>

            {{--
            <li class="navbar-item">
                <a class="nav-link" href="{{ route('contact') }}">Contact us</a>
            </li>
            --}}

            <li class="navbar-item">
                <a class="nav-link" href="{{ route('buy') }}">Where to buy</a>
            </li>
        </ul>
    </div>
</nav>
