<footer>
    <div class="social-link-container">
        <a href="https://www.facebook.com/pearldrops/" target="_blank" class="social-link bTrack" data-track="Facebook" data-type="Footer Social">
            <img src="img/fb.png" alt="">
        </a>
        {{--
            <a href="https://twitter.com/PearlDropsUK" target="_blank" class="social-link bTrack" data-track="Twitter" data-type="Footer Social">
            <img src="img/twitter.png" alt="">
        </a>
        --}}
        <a href="https://www.instagram.com/pearldropsuk/" target="_blank" class="social-link bTrack" data-track="Instagram" data-type="Footer Social">
            <img src="img/instagram.png" alt="">
        </a>
        {{--
            <a href="https://www.youtube.com/user/PearlDropsUK" target="_blank" class="social-link bTrack" data-track="YouTube" data-type="Footer Social">
            <img src="img/youtube.png" alt="">
        </a>
        --}}
    </div>
    <div class="terms-section">
        <a href="{{ route('terms') }}">T&amp;Cs</a> |
        <a href="{{ route('cookie-notice') }}">Cookie Notice</a> |
        <a href="{{ route('privacy-policy') }}">Privacy Policy</a> |
        {{-- <a href="{{ route('contact') }}">Contact us</a> | --}}
        <a href="http://www.churchdwight.co.uk" target="_blank">Visit C&amp;D</a><br>
<!-- OneTrust Cookies Settings button start -->
<button id="ot-sdk-btn" class="ot-sdk-show-settings">Cookie Settings</button>
<!-- OneTrust Cookies Settings button end -->
        <div class="copyright">&copy; Copyright Church &amp; Dwight UK Ltd. 2021. All rights reserved. Pearl Drops is a registered trademark of Church &amp; Dwight Co., Inc.</div>
    </div>
</footer>