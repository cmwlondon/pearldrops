@foreach (__('products.buy-links-'.$productId) as $idx => $retailer)
	@if ($retailer != '')
		<div class="logo-wrap"><a href="{{{$retailer}}}" class="bTrack" target="_blank" data-track="{{ __('products.product-name-'.$productId) }} : {{ __('products.retailer-'.$idx) }}" data-type="buy-now"><img class="logo-image" src="/img/buy-now/{{{$idx}}}.svg"></a></div>
	@endif
@endforeach