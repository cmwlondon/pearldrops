<!--Start social media embeds-->
<section class="jumbotron jumbotron-fluid social-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>This is social</h2>
                <p>There’s nothing more beautiful than an authentic smile. Share yours at #PDRealSmiles.</p>
            </div>
        </div>

        <div class="row social-photo-container clearfix instagram">
            @if (count($instagram) > 0)
                @for ($i = 1; $i <= 4; $i++)
                    <div class="col-md-3 col-sm-6 col-6 no-pad block" data-id="{{$i}}" data-show="{{$i}}">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA0VpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQyIDc5LjE2MDkyNCwgMjAxNy8wNy8xMy0wMTowNjozOSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6N0I5REM2N0QxQjQ3MTFFODhCRkFERTMwNjJBOUEwMjkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6N0I5REM2N0MxQjQ3MTFFODhCRkFERTMwNjJBOUEwMjkiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChXaW5kb3dzKSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJhZG9iZTpkb2NpZDpwaG90b3Nob3A6MTYwZDk1M2YtM2NkNy1hMzQ5LThiNDYtOGYxNmU1MzliYTY2IiBzdFJlZjpkb2N1bWVudElEPSJhZG9iZTpkb2NpZDpwaG90b3Nob3A6MTYwZDk1M2YtM2NkNy1hMzQ5LThiNDYtOGYxNmU1MzliYTY2Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8++09afwAAABBJREFUeNpi+P//PwNAgAEACPwC/tuiTRYAAAAASUVORK5CYII=" class="shim"/>
                        
                        @foreach($instagram as $idx => $image)
                            <div class="gram" data-id="{{($idx+1)}}">
                                {{-- <a href="{{$image->link}}" target="instagram"> --}}
                                @if ($image->type == 'video')
                                <img src="{{{$image_path}}}/button-video-play.png" class="btn-play" alt="" data-rjs="1"/>
                                @endif
                                @if ($image->type == 'video' && $image->black == true)
                                <img src="" data-src="{{$image_path}}/video-placeholder.jpg" class="ig" alt="" data-rjs="1"/>
                                @else
                                {{-- <img src="" data-src="/storage/instagram/{{$image->media_local}}" class="ig" alt=""/> --}}
                                <img src="/storage/instagram/large_{{$image->media_local}}" srcset="/storage/instagram/small_{{$image->media_local}} 960w,/storage/instagram/medium_{{$image->media_local}} 1280w,/storage/instagram/large_{{$image->media_local}} 1920w" alt="View on Instagram" class="ig"/>
                                @endif
                                {{-- </a> --}}
                            </div>
                        @endforeach
                    </div>
                @endfor
            @else
                @for ($i = 1; $i <= 4; $i++)
                    <div class="col-md-3 col-sm-6 col-6 no-pad block" data-id="{{$i}}" data-show="{{$i}}">
                        <img src="/img/placeholder.png" alt="">
                    </div>
                @endfor
            @endif
        </div>
    </div>
</section>
<!--End social media embeds-->