<!doctype html>
<html class="no-js" lang="en">
	<head>
<!-- OneTrust Cookies Consent Notice start for pearldrops.co.uk -->
<script src="https://cdn.cookielaw.org/scripttemplates/otSDKStub.js"  type="text/javascript" charset="UTF-8" data-domain-script="641543fb-fb73-450a-80df-dca626ff04cd" ></script>
<script type="text/javascript">
function OptanonWrapper() { }
</script>
<!-- OneTrust Cookies Consent Notice end for pearldrops.co.uk -->
		@include('main.layouts.partials._analytics')

		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

		<!-- CSRF Token -->
    	<meta name="csrf-token" content="{{ csrf_token() }}">

		<title>{{{$meta['title']}}}</title>
		<meta name="description" content="{{{$meta['desc']}}}" />
		<meta name="keywords" content="{{{$meta['keywords']}}}"/>
		<meta name="keyphrases" content="{{{$meta['keyphrases']}}}"/>
		
		<link href="https://fonts.googleapis.com/css?family=Oswald|Source+Sans+Pro" rel="stylesheet">
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

		@if (isset($pageViewCSS) && count($pageViewCSS) > 0)
	        @foreach($pageViewCSS as $stylesheet)
				<link rel="stylesheet" href="{{{$stylesheet}}}?t={{ time() }}" />
	        @endforeach
	    @endif
		{{--
		@if (isset($pageViewCSS) && count($pageViewCSS) > 0)
	        @foreach($pageViewCSS as $stylesheet)
	            @if (App::environment() == 'local')
	            	<link rel="stylesheet" href="{{{$stylesheet}}}?{{{$version}}}" toRefresh="{{{$stylesheet}}}"/>
	            @else
	            	<link rel="stylesheet" href="{{{$stylesheet}}}?{{{$version}}}"/>
	            @endif
	        @endforeach
	    @endif
		--}}

		<meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
		<meta http-equiv="Content-Language" content="en-gb, English" />
		<meta http-equiv="Content-Style-Type" content="text/css" />

		<!-- Chrome Frame -->
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<!-- For Facebook -->
		<meta property="og:locale" content="en_GB" />
		<meta property="og:type" content="{{{$meta['pagetype']}}}" />
		<meta property="og:title" content="{{{$meta['title']}}}" />
		<meta property="og:description" content="{{{$meta['desc']}}}" />
		<meta property="og:url" content="{{{$meta['link']}}}" />
		<meta property="og:site_name" content="Pearl Drops"/>
		<meta property="og:image" content="{{{$meta['image']}}}?{{{$version}}}" />
		<meta property="fb:app_id" content="{{{$meta['fbid']}}}"/>

		<meta name="format-detection" content="telephone=no">
		
		@if (isset($nocache))
			<meta name="robots" content="noindex, nofollow, noarchive"/>
		@endif
		
		@if (isset($canonical))
		<link rel="canonical" href="{{{ $canonical }}}">
		@else
		<link rel="canonical" href="{{{ url()->current() }}}">
		@endif
		
		@include('main.layouts.partials._favicon')
	</head>

	<body class="{{{ $section }}}" data-mobile-menu="closed">
		<a id="top"></a>

		{{-- @include('main.layouts.partials._disclaimer') --}}
		
		@include('main.layouts.partials._main-menu')
		@include('main.layouts.partials._mobile-menu')

		<div id="page">
			@yield('content')

			@include('main.layouts.partials._footer')
		</div>

		@yield('components')

		<script>
			@if (isset($jsVars))
				@foreach ($jsVars as $key => $var)
					var {{$key}} = "{{ $var }}";
				@endforeach
			@endif
			@if (isset($pageViewJS))
				var pageViewJS = "{{ $pageViewJS }}";
			@endif
		</script>

		{!! Html::script('js/libs/requirejs/require.js', ['data-main' => '/js/main/main.min.js?'.$version]) !!}
	</body>
</html>
