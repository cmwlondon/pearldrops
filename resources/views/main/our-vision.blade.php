@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<!--Hero section-->
    <section class="hero about-hero">
        <div class="hero-content-container">
            <div class="white-bg-container">
                <h1>This is our vision</h1>
                <p>Our vision at Pearl Drops is simple. We want every woman to feel special and confident in their every day. It’s a vision that motivates us to do what we do – to help women achieve their best natural beauty so they can feel inspired to live, laugh&nbsp;and&nbsp;love.</p>
            </div>
        </div>
    </section>
    <!--End carousel section-->
    <!--Start split screen section-->
    <article class="split-screen-section">
        <!--Our mission split-->
        <section class="split-screen-item image-panel our-mission-image-panel">
            <div class="split-screen-content">

            </div>
        </section>
        <section class="split-screen-item our-mission-content-panel">
            <div class="split-screen-content">
                <h2 class="extra-large-title">This is our mission</h2>
                <p>There is more than one shade of white. There’s pure white, there’s brilliant white, there’s sensitive white and there’s natural white. Just to name a few… That’s why, over decades of trials and innovations, we constantly strive to innovate clinically tested products that are tried, tested and tailored to meet the individual needs of women across the world. So, if you want to dazzle with a brilliant Hollywood smile, enhance the beauty of your natural smile or simply protect against a glass of red wine, we’ve got a white for&nbsp;you.</p>
            </div>
        </section>
        <!--Our campaign split row one-->
        <section id="campaign" class="split-screen-item about-our-campaign-content-panel">
            <div class="split-screen-content">
                <h2>THIS IS OUR BELIEF</h2>
                <p>No two smiles are the same. Every woman’s teeth are unique and that’s what makes them&nbsp;beautiful.
                    </br>
                    </br>
                    Some are wonky.</br>
                    Some are chipped.</br>
                    Some are big.</br>
                    Some are small.</br>
                    </br>
                    All are beautiful.
                </p>
            </div>
        </section>
        <section class="split-screen-item image-panel our-campaign-image-panel-1">
            <div class="split-screen-content">

            </div>
        </section>
        <!--Our campaign split row two-->
        <section class="split-screen-item image-panel our-campaign-image-panel-2">
            <div class="split-screen-content">

            </div>
        </section>
        <section class="split-screen-item about-our-campaign-content-panel">
            <div class="split-screen-content">
                <p>Except, advertising doesn’t seem to agree. Time and time again, we see beautiful supermodels with millimetre perfect veneers. That’s why, to combat a history of misleading advertising and promote positive body confidence, we’re leading the charge of change in our ‘This is Real’ campaign. There’s no Photoshop trickery, no veneered supermodels and definitely no false teeth, (yes, this does really happen). Just real women, real smiles and real whitening. See the film that kicked it all off&nbsp;below. </p>
            </div>
        </section>
    </article>
    <!--End split screen section-->

    {{-- @include('main.layouts.partials._video') --}}

    <!--Start split screen section-->
    <article class="split-screen-section">
        <section class="split-screen-item image-panel our-history-image-panel">
            <div class="split-screen-content">

            </div>
        </section>
        <section class="split-screen-item our-history-content-panel">
            <div class="split-screen-content">
                <h2 class="extra-large-title">This is our history</h2>
                <p>Over the last 50 years (and counting), our professional Research and Development team have worked tirelessly – combining individual brilliance with shared passion – to innovate whitening products that are tried, tested and tailored to meet the individual needs of women across the world. Read all about it on our history&nbsp;page:</p>
                <a href="{{ route('history') }}" class="ghost-button ghost-button-white">View our history</a>
            </div>
        </section>
    </article>

    @include('main.layouts.partials._social')
@endsection

@section('components')
	
@endsection