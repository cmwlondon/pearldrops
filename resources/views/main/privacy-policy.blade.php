@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<!--Hero section-->
    <section class="page-intro container">
        <div class="row">
            <div class="col-md-6">
                <h1>CHURCH &amp; DWIGHT EU CUSTOMER PRIVACY STATEMENT</h1>
            </div>
        </div>
    </section>
    <!--Start content -->
    <section class="container">
        <div class="row">
            <div class="col-sm-12">

<p>Church &amp; Dwight UK Limited (Church &amp; Dwight”, “we” or “us”), have established this Privacy Policy which describes the types of personal data we collect about you online (via this website or app) as well as offline, how and for what purposes we use your personal data, with whom we share it, how long we retain it and the choices and rights available to you in relation to our use of your personal data.</p>
<p>For the purposes of applicable data protection law, the data controller under this Customer Privacy Policy is Church &amp; Dwight.</p>
 
<h3>Information We collect About You</h3>

<p>When using this website, we may automatically collect information from you using cookies. To the extent that this website uses cookies, please see our cookie banner or <a href="/cookie-notice" class="underline">Cookie Notice page</a> for more information on the use of cookies.</p>
<p>Further, we collect, store and further use your personal data if you provide it to us, e.g., in the course of a purchase, when you sign up to receive our newsletter or other marketing communications, when you sign up for our online coupon program, when you contact us with a question, request, comment or complaint, when you enter a contest or promotion or when you complete a survey (both online and offline). The types of personal data we may obtain about you include:</p>
<ul class="normal">
    <li>Contact information (such as your name, address, phone number, e-mail address and birth date);</li>
    <li>Payment information (such as your payment card number and expiration date);</li>
    <li>Location and geographic information if you use our mobile site or app and have turned on the location services;</li>
    <li>Information you may provide us in the case of a claim or complaint about our products, including health-related data; and</li>
    <li>Other content you may provide (such as reviews, survey responses, question answers and comments).</li>
</ul>
<p>We will let you know which information is required or optional (on the form on which we collect your data or, where applicable, orally). Failure to provide the information required may prevent us from responding to you or providing you with the product or service you request.</p>

<p>If you make a purchase on this site, we will also create and keep a record of your orders, purchase history, and shopping behavior and preferences based on the information you provided in connection with your purchase(s).</p>
<p>The personal data collected through this site or app may be combined and maintained with personal data we collect from you offline or from third parties for the purposes described below.</p>
  
<h3>How We use Your Information</h3>
<p>Typically, we use the personal data we collect about you for the following purposes:</p>
<ul class="normal">
    <li>to create and manage your account;</li>
    <li>to manage your orders, deliveries and invoices, and process your payments;</li>
    <li>to notify you about contests and promotional events, administer your participation in such contests and events, and notify you in the event that you are a winner of any such contest or event;</li>
    <li>to conduct polls, surveys and consumer testing;</li>
    <li>to manage and communicate with you about our loyalty programs;)</li>
    <li>if you so elect, to send you our newsletter or other marketing or promotional communications;</li>
    <li>to tailor our advertisements and offers to your interests based on the record of your orders, purchase history and shopping behavior and preferences;</li>
    <li>to respond to your questions, requests or comments;</li>
    <li>to handle claims or complaints we receive in connection with our products and services and to enforce our legal claims;</li>
    <li>to contact you with reminders and updates;</li>
    <li>to perform accounting, auditing and other internal functions;</li>
    <li>for statistical and research purposes; and</li>
    <li>to comply with our legal obligations, including our obligation to monitor the safety of our products and report any issue as well as any preservation or disclosure obligation we may have in anticipation or in the context of legal proceedings</li>
</ul>

<p>We will use your personal data for these purposes based on one of the following grounds: if we have obtained your consent, or, as the case may be, if it is necessary to perform a contract to which you are party or to take steps at your request prior to entering into a contract with us, if it is necessary to comply with a legal obligation to which we are subject, or if we have a legitimate interest to do so (including a legitimate interest in processing and enforcing legal claims, and performing marketing activities, data statistics, market and consumer research and other analyses, as well as audits and searches of documents, e.g., in response to requests for litigation hold or pre-trial discovery requests).</p>
 
<h3>Marketing</h3>
<p>We may use your electronic contact details, or permit our brands to use your electronic contact details, to provide you with promotional e-mails or text messages about Church &amp; Dwight and/or our http://www.churchdwight.co.uk/brands. If you have consented to receive these e-mails/text messages or, as the case may be, if you have not opted out. You can opt out of receiving e-mail/text communications from us or our brands at any time by following the instructions which are contained in all e-mail/text communications or contacting us as specified in the How to Contact Us section below.</p>

<p>With Whom We share Your Information</p>

<p>Your personal data will be processed by authorized personnel of Church &amp; Dwight only on a need-to-know basis, including Church &amp; Dwight’s customer relations, marketing, finance, accounting, legal, IT and regulatory or compliance services.</p>
<p>We will not rent, sell or exchange your personal data with any third party for their marketing purposes. We will, however, share your personal data with:</p>
<ul class="normal">
    <li>other group companies for the purposes described above; and</li>
    <li>our third party service providers who perform specific business support services on our behalf and based on our instructions. The provision of these services may involve limited access to your personal data. We require these companies to use the personal data only to provide the contracted services and prohibit them from transferring the information to another party except as needed to provide the contracted services. Examples of such business support services include fulfilling coupons, sending e-mails, conducting and administering contests and promotions, executing surveys, managing customer relations and in case of purchases made on our site, processing card payments and/or shipping or fulfilling orders.</li>
</ul>
<p>We will disclose your personal data to other parties, only with your consent, unless when we are required to do so or permitted to do so by law, regulation, legal process or enforceable government request. In particular, we may disclose your personal data to regulatory authorities to comply with our reporting obligations and to adverse parties in litigation or governmental entities in accordance with applicable law.</p>
<p>We also reserve the right to transfer personal data we hold about you in the event we sell or transfer all or a portion of our business or assets (including in the event of a reorganization, dissolution or liquidation). The recipient of personal data following such actions may have privacy policies that differ from those in this policy.</p>
 
<h3>Data Transfers</h3>
<p>Certain of the above recipients are located in countries other than the country where the data originally was collected, including outside the European Union. In particular, we may transfer your personal data to other group companies, including our parent company in the U.S. and sister companies in Australia, Canada, Brazil, China and Mexico to manage customer functions globally within the group. Duly authorized personnel of our group companies may have access to all your data for this purpose on a need-to-know basis only. In addition, if you contact our consumer relations department about our products, we will transfer your personal data to our U.S. service provider that provides consumer relations management services on our behalf. This service provider will have access only to your contact information and complaint or query-related data for this purpose. The laws in the above countries (except Canada) have not been recognized by the European Commission as providing for an adequate level of data protection. Church &amp; Dwight concluded data transfer agreements incorporating the European Commission’s Standard Contractual Clauses to ensure such a level. You may obtain a copy of these agreements by contacting us as specified in the How to Contact Us section below. In France, the data transfers were authorized by the CNIL (Decisions No. DF-2017-2358 and DF-2017-2359).</p>
 
<h3>How We Protect Your Information</h3>
<p>We maintain administrative, technical and physical safeguards designed to protect your personal data against accidental or unlawful destruction, loss, alteration, access, unauthorized disclosure or access. Services providers who might have access to your personal data in order to provide services on our behalf will be contractually obliged to keep such personal data in confidence and provide adequate data security measures.</p>
 
<h3>How Long We Retain Your Information</h3>
<p>Typically, we keep your personal data for the duration of our relationship, plus a reasonable period in order to be able to run regular blocking/deletion routines, or to take into account the applicable statute of limitation period or if required under mandatory applicable law. Your personal data may be kept for a shorter or longer period depending on the purposes for which your data were collected.</p>
 
<h3>Children</h3>
<p>Our site or app is intended for visitors over eighteen years of age. We do not target our site/app to appeal to children under the age of thirteen and we do not knowingly collect personal information from children under the age of thirteen.</p>
 
<h3>Your Rights</h3>
<p>In accordance with applicable data protection law, you have the right to request access to and rectification or erasure of your personal data. You also have the right to request that the processing of your personal data is restricted or to object to such processing if permitted by applicable law. If you have consented to our use of your personal data, you have the right to withdraw your consent at any time with effect for the future and without providing reasons. Where applicable, you have the right to receive the personal data, that you provided to us, in a structured, commonly used and machine-readable format and to transmit those data to another data controller. You may also define the ways how your personal data will be processed after your death, where permitted by applicable law. If you wish to exercise these rights, please click <a href="https://privacyportal.onetrust.com/webform/7d0a54b5-0170-4a32-9dea-c412dfabf292/937fe9aa-5a4d-45f7-a71c-64ac8cb78ab6" target="_blank" title="https://privacyportal.onetrust.com/webform/" class="underline">here</a>. You may also lodge a complaint with a data protection authority.</p>
 
<h3>Links to Other Websites</h3>
<p>Our site or app may contain links to websites operated and maintained by third parties, including those of online retailers that sell our products. The privacy policies on these third party websites may be different from this policy and we are not responsible for the information collection practices or the content of the third party sites to which we link. You access such linked sites at your own risk and should read the privacy policy of any linked site before sharing your personally identifiable information on such site.</p>
  
<h3>Changes to this Privacy Policy</h3>
<p>We may amend this Privacy Policy from time to time. If we update or change the terms of this policy, we will post changes in a revised Privacy Policy available on this site.</p>
  
<h3>How to Contact Us</h3>
<p>If you have any questions about this Privacy Policy or if you would like to exercise your rights, you may contact us at:</p>
<p><strong>Church &amp; Dwight UK Limited, Wear Bay Road, Folkestone, Kent, CT19 6PG, UK, Attention: Consumer Relations or e-mail address: <a href="mailto:privacychd@churchdwight.com">privacychd@churchdwight.com</a></strong></p>


            </div>
        </div>
    </section>
    <!--End content-->

    @include('main.layouts.partials._social')
@endsection

@section('components')
	
@endsection