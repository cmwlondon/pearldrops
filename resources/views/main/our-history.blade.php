@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<!--Hero section-->
    <section class="hero history-hero">
        <div class="hero-content-container">
            <div class="white-bg-container">
                <h1>This is </br>our history</h1>
                <p>Over the last 50 years (and counting), our Research and Development team have worked tirelessly – combining brilliance with passion - to innovate whitening products that are tried, tested and tailored to meet the individual needs of women across the&nbsp;world.</p>
            </div>
        </div>
    </section>
    
    <section class="panel-grid-container panel-grid-container-desktop container d-none d-lg-block">
        <div class="d-flex flex-row row">
            <div class="col-md-4 panel-item image-panel history-item-1"></div>
            <div class="col-md-4 panel-item history-item-2"><p><span class="date">1968</span><strong>The year was 1968. The man of the moment?</strong> A dentist who – after his wife bemoaned the lack of home whitening products – saw an incredible opportunity to change the world of smiles forever. In one illuminating lightbulb moment and many hours of hard work, he developed Pearl Drops Whitening Polish. The rest, they say, ‘is history’.</p></div>
            <div class="col-md-4 panel-item history-item-3"><p><span class="date">1972</span><strong>In four short years after the birth of Pearl Drops’</strong> innovating tooth whitening polish, Pearl Drops had already become a favourite amongst America’s rich and famous. It proved so popular that convincing pop culture idol of the time, Tina Turner, to feature in a television advert was a breeze. The ad proved just as effective as the product itself, launching the brand into the public eye… and into bathroom cabinets across the country.</p></div>
        </div>
        <div class="d-flex row flex-row-reverse">
            <div class="col-md-4 panel-item history-item-4"><p><span class="date">1973</span><strong>It didn’t take long for Pearl Drops to become</strong> a major success across the 50 United States of America, and that success didn’t stop there. Within – a rather amazingly short – five years, the brand had launched onto the international stage, making the leap across the pond to whiten smiles in the UK for the first time.</p></div>
            <div class="col-md-4 panel-item image-panel history-item-5"></div>
            <div class="col-md-4 panel-item history-item-6"><p><span class="date">1978</span><strong>Women in the US and the UK</strong> had begun to use Pearl Drops as part of their everyday whitening regime.</p></div>
        </div>
        <div class="d-flex flex-row row">
            <div class="col-md-4 panel-item history-item-7"><p><span class="date">2006</span><strong>34 years after the famous advert with Tina Turner</strong>, Pearl Drops continues to be a favourite amongst celebrities with Holly Willoughby and Mel Sykes proudly proclaiming themselves as avid admirers of the brand. Holly said, “Pearl Drops is already part of my beauty regime and has become the must have product for when I hit the town. It makes my teeth shine and look brighter so I can smile with confidence!”</p></div>
            <div class="col-md-4 panel-item history-item-8"><p><span class="date">2011</span><strong>2011 saw the launch of our UK Facebook page</strong>, culminating in a massive online event known as the White Wedding. PD fans were put in the driver’s seat, choosing everything from the wedding cake through to the bride’s dress – it truly was a day to remember!</p></div>
            <div class="col-md-4 panel-item image-panel history-item-9"></div>
        </div>
        <div class="d-flex row flex-row-reverse">
            <div class="col-md-4 panel-item history-item-10"><p><span class="date">2012</span><strong>2012 was another huge year for Pearl Drops.</strong> This time, the endless pursuit of innovation spelled doomsday for dentists charging upwards of £600 for professional teeth whitening. The catalyst? Pearl Drops new Beauty Sleep. It was a first of its kind – a product that was clinically proven to whiten teeth when you sleep. It was a dream for the brand and women everywhere.</p></div>
            <div class="col-md-4 panel-item image-panel history-item-11"></div>
            <div class="col-md-4 panel-item history-item-12"><p><span class="date">2017</span><strong>Twenty seventeen was a very exciting year for the Pearl Drops brand.</strong> They launched a world first – a new advertisement campaign that showed off the teeth of real women. Not models. Not retouched. Cracks, chips, stains and all.</p></div>
        </div>
    </section>

    <section class="panel-grid-container container d-lg-none d-xl-none">
        <div class="d-flex flex-row row">
            <div class="col-md-6 col-sm-6 col-12 panel-item image-panel history-item-1"></div>
            <div class="col-md-6 col-sm-6 col-12 panel-item history-item-2"><p><span class="date">1968</span><strong>The year was 1968. The man of the moment?</strong> A dentist who – after his wife bemoaned the lack of home whitening products – saw an incredible opportunity to change the world of smiles forever. In one illuminating lightbulb moment and many hours of hard work, he developed Pearl Drops Whitening Polish. The rest, they say, ‘is history’.</p></div>
        </div>
        <div class="d-flex row flex-row-reverse">
            <div class="col-md-6 col-sm-6 col-12 panel-item history-item-3"><p><span class="date">1972</span><strong>In four short years after the birth of Pearl Drops’</strong> innovating tooth whitening polish, Pearl Drops had already become a favourite amongst America’s rich and famous. It proved so popular that convincing pop culture idol of the time, Tina Turner, to feature in a television advert was a breeze. The ad proved just as effective as the product itself, launching the brand into the public eye… and into bathroom cabinets across the country.</p></div>
            <div class="col-md-6 col-sm-6 col-12 panel-item history-item-4"><p><span class="date">1973</span><strong>It didn’t take long for Pearl Drops to become</strong> a major success across the 50 United States of America, and that success didn’t stop there. Within – a rather amazingly short – five years, the brand had launched onto the international stage, making the leap across the pond to whiten smiles in the UK for the first time.</p></div>
        </div>
        <div class="d-flex flex-row row">
            <div class="col-md-6 col-sm-6 col-12 panel-item image-panel history-item-5"></div>
            <div class="col-md-6 col-sm-6 col-12 panel-item history-item-6"><p><span class="date">1978</span><strong>Women in the US and the UK</strong> had begun to use Pearl Drops as part of their everyday whitening regime.</p></div>
        </div>
        <div class="d-flex row flex-row-reverse">
            <div class="col-md-6 col-sm-6 col-12 panel-item history-item-7"><p><span class="date">2006</span><strong>34 years after the famous advert with Tina Turner</strong>, Pearl Drops continues to be a favourite amongst celebrities with Holly Willoughby and Mel Sykes proudly proclaiming themselves as avid admirers of the brand. Holly said, “Pearl Drops is already part of my beauty regime and has become the must have product for when I hit the town. It makes my teeth shine and look brighter so I can smile with confidence!”</p></div>
            <div class="col-md-6 col-sm-6 col-12 panel-item history-item-8"><p><span class="date">2011</span><strong>2011 saw the launch of our UK Facebook page</strong>, culminating in a massive online event known as the White Wedding. PD fans were put in the driver’s seat, choosing everything from the wedding cake through to the bride’s dress – it truly was a day to remember!</p></div>
        </div>
        <div class="d-flex flex-row row">
            <div class="col-md-6 col-sm-6 col-12 panel-item image-panel history-item-9"></div>
            <div class="col-md-6 col-sm-6 col-12 panel-item history-item-10"><p><span class="date">2012</span><strong>2012 was another huge year for Pearl Drops.</strong> This time, the endless pursuit of innovation spelled doomsday for dentists charging upwards of £600 for professional teeth whitening. The catalyst? Pearl Drops new Beauty Sleep. It was a first of its kind – a product that was clinically proven to whiten teeth when you sleep. It was a dream for the brand and women everywhere.</p></div>
        </div>
        <div class="d-flex row flex-row-reverse">
            <div class="col-md-6 col-sm-6 col-12 panel-item image-panel history-item-11"></div>
            <div class="col-md-6 col-sm-6 col-12 panel-item history-item-12"><p><span class="date">2017</span><strong>Twenty seventeen was a very exciting year for the Pearl Drops brand.</strong> They launched a world first – a new advertisement campaign that showed off the teeth of real women. Not models. Not retouched. Cracks, chips, stains and all.</p></div>
        </div>
    </section>
    <div class="container">
        <div class="row divider-row">
            <div class="col-md-12">
                <hr>
            </div>
        </div>
    </div>

    @include('main.layouts.partials._social')
@endsection

@section('components')
	
@endsection