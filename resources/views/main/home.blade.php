@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<!--Carousel section-->
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item this-is-innovation-item active">
                <img alt="MORE THAN 50 YEARS OF WHITENING INNOVATION" src="img/fifty-years.png" class="fifty">
                <div class="carousel-content-container">
                    <div class="carousel-content">
                        <h2 class="carousel-title">This is innovation</h2>
                        <p>Find out how 50 years of whitening innovation and clinical studies has made our products better than the&nbsp;rest.</p>
                        <a class="ghost-button" href="{{ route('science') }}">Find out more</a>
                    </div>
                </div>
            </div>

            <div class="carousel-item all-products-item">
                <img alt="MORE THAN 50 YEARS OF WHITENING INNOVATION" src="img/fifty-years.png" class="fifty">
                <div class="carousel-content-container">
                    <div class="carousel-content">
                        <h2 class="carousel-title">This is </br>pearl drops</h2>
                        <p>Hollywood glamour, sensitive stain removal or natural lift? There is more than one choice when it comes to the shade of white that&rsquo;s right for you. Discover your perfect&nbsp;match.</p>
                        <a class="ghost-button" href="{{ route('collection') }}">Discover the range</a>
                    </div>
                </div>
                <div class="carousel-image-container">
                    <img src="/img/all-products-lg.jpg" class="carousel-image" alt="The Pearl Drops Collection">
                </div>
            </div>

            <div class="carousel-item this-is-real-item">
                <img alt="MORE THAN 50 YEARS OF WHITENING INNOVATION" src="img/fifty-years.png" class="fifty">
                <div class="carousel-content-container">
                    <div class="carousel-content">
                        <img src="/img/this-is-real-logo.png" alt="this is real logo">
                        <p class="white pt-5">Real models. Real photos. Real smiles. There&rsquo;s no such thing as imperfections – just different shades of beautiful. Discover why we stand for Real&nbsp;Whitening.</p>
                        <a class="ghost-button ghost-button-white" href="{{ route('vision') }}#campaign" style="margin-left:15px;">Find out more</a>
                    </div>
                </div>
            </div>
            
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!--End carousel section-->
    <!--Start split screen section-->
    <article class="split-screen-section home-split-screen-section">
        <!--This tile is hidden on mobile and positioned below to solve repeating image issue-->
        <section class="split-screen-item image-panel our-collection-image-panel d-none d-sm-flex">
            <div class="split-screen-content full-range-zoom">
                <img src="/img/full-range-hero.jpg" alt="Our collection">
            </div>
        </section>
        <section class="split-screen-item our-collection-content-panel">
            <div class="split-screen-content">
                <h2>This is our collection</h2>
                <p>A formula for every smile. Explore our extensive range of specialist and professional whitening&nbsp;solutions.</p>
                <a href="{{ route('collection') }}" class="ghost-button ghost-button-white">See the collection</a>
            </div>
        </section>
        <section class="split-screen-item image-panel our-collection-image-panel d-flex d-sm-none">
            <div class="split-screen-content">
                <img src="/img/full-range-hero.jpg" alt="Our collection">
            </div>
        </section>
        <section class="split-screen-item image-panel our-campaign-image-panel">
            <div class="split-screen-content">

            </div>
        </section>
        <section class="split-screen-item our-campaign-content-panel">
            <div class="split-screen-content">
                <h2>This is our belief</h2>
                <p>Learn all about our latest campaign and why we&rsquo;re passionate about real&nbsp;smiles.</p>
                <a href="{{ route('vision') }}#campaign" class="ghost-button ghost-button-white">See the campaign</a>
            </div>
        </section>
        <section class="split-screen-item our-history-content-panel">
            <div class="split-screen-content">
                <h2>This is our history</h2>
                <p>Read all about the history of the UK&rsquo;s original Specialist Whitening brand and our journey over the last&nbsp;50&nbsp;years.</p>
                <a href="{{ route('history') }}" class="ghost-button ghost-button-white">See our history</a>
            </div>
        </section>
        <section class="split-screen-item image-panel our-history-image-panel">
            <div class="split-screen-content">

            </div>
        </section>
    </article>
    <!--End split screen section-->

    {{-- @include('main.layouts.partials._video') --}}

    @include('main.layouts.partials._social')
@endsection

@section('components')
	
@endsection