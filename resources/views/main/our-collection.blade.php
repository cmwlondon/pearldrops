@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<!--Hero section-->
    <section class="page-intro container page-intro-container-collection">
        <div class="container collection-inner">
            <div class="row">
                <div class="col-lg-5 col-xl-4">
                    <h1>This is </br>Pearl Drops</h1>
                    <p class="xl-text large-p">Hollywood brilliant or natural glow?</br>There is more than one shade of white.</br>Choose the white that works for you.</p>
                </div>
                <div class="col-lg-7 col-xl-8 page-intro-image">
                    <img src="/img/full-range-hero.jpg" alt="Pearl Drops Full Range">
                </div>
            </div>
            <div class="row small-divider-row">
                <hr class="w-100">
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-12 text-center rectangular-tile dark-blue-bg">
                    <img src="/img/collection/specialist-white.jpg" alt="Specialist White" class="packs-group"/>
                    <h2>PROFESSIONAL DAILY WHITENING TOOTHPASTES</h2>
                    <p class="regular-p">Our latest clinically proven innovations for daily use, with ultimate results.</p>
                    <a href="#lasting-flawless-white" class="ghost-button ghost-button-white btn-anchor">Explore the range</a>
                </div>
                <div class="col-md-6 col-sm-12 text-center rectangular-tile grey-bg">
                    <img src="/img/collection/professional-white.jpg" alt="Professional White" class="packs-group"/>
                    <h2>PROFESSIONAL WHITENING TREATMENTS</h2>
                    <p class="regular-p">Professional, dentist-inspired whitening treatments.</p>
                    <a href="#professional-whitening-treatment" class="ghost-button btn-anchor">Explore the range</a>
                </div>
            </div>
        </div>
    </section>
    <section class="container collection-item-container" id="lasting-flawless-white">
        <div class="container collection-inner">
            <div class="row">
                <div class="col-md-6">
                    <img src="/img/collection/lasting-flawless-white.png" class="collection-product" alt="Lasting Flawless White">
                </div>
                <div class="col-md-6">
                    <h2 class="label-heading pb-4">Daily Whitening Toothpastes</h2>
                    <h2 class="collection-item-title pb-4">Remove 100% more surface stains</br><span class="light-text">with lasting flawless white</span></h2>
                    <p>Pearl Drops Lasting Flawless White has been specially developed by our whitening experts to deliver superior stain removal and protection with a gentle, low abrasion formula. Liquid Calcium works to repair the tooth’s natural surface, smoothing imperfections and improving shine for outstanding whitening results. Used daily, it can help protect your whiteness for&nbsp;longer.</p>
                    <p><strong>Outstanding Whiteness in 3 days that&nbsp;lasts*.</strong></p>
                    <div class="review-container pb-5">
                        
                        <a href="http://www.boots.com/pearl-drops-lasting-flawless-white-75ml-10190782#BVRRContainer" target="_blank" rel="nofollow" data-track="lasting-flawless-white" data-type="reviews" class="underline-link bTrack">Read reviews</a>
                    </div>
                    <div class="collection-social-container w-100" data-ref="lasting-flawless-white">
                        <div class="share-label d-inline-block mt-4 mr-3">Share</div>
                        <a href="#" class="collection-social-link d-inline bTweet"><img src="/img/twitter-square.png" alt="Twitter Logo">    </a>
                        
                        <a href="#" class="collection-social-link d-inline bShareFB"><img src="/img/fb-square.png" alt="Facebook Logo"></a>
                    </div>
                    <a href="/pdf/Pearl-Drops-Lasting-Flawless-White.pdf" target="_blank" class="underline-link w-100 d-block mt-4">Product info</a>
                    <div class="buy-group">
                        <a href="#" class="buy-expand ghost-button">Buy now</a>
                        <div class="popup-menu-wrap">
                            <div class="popup-menu">
                                <div class="logo-wrap"><a href="http://www.boots.com/pearl-drops-lasting-flawless-white-75ml-10190782" class="bTrack" target="_blank" data-track="Lasting Flawless White : Boots" data-type="buy-now"><img class="logo-image mh" src="/img/boots-logo.png"></a></div>
                <div class="logo-wrap"><a href="https://www.superdrug.com/Toiletries/Dental/Whitening-Toothpaste/Pearl-Drops-Flawless-White-75ml/p/405700" class="bTrack" target="_blank" data-track="Lasting Flawless White : SuperDrug" data-type="buy-now"><img class="logo-image" src="/img/superdrug-logo.png"></a></div>
                <div class="logo-wrap"><a href="https://www.tesco.com/groceries/en-GB/products/291195749" class="bTrack" target="_blank" data-track="Lasting Flawless White : Tesco" data-type="buy-now"><img class="logo-image" src="/img/tesco-logo.png"></a></div>
                <div class="logo-wrap"><a href="https://www.sainsburys.co.uk/shop/gb/groceries/pearl-drops-lasting-flawless-white-toothpolish-75ml" class="bTrack" target="_blank" data-track="Lasting Flawless White : Sainsburys" data-type="buy-now"><img class="logo-image" src="/img/sainsburys-logo.png"></a></div>
                {{-- <div class="logo-wrap"><a href="https://www.lookfantastic.com/pearl-drops-lasting-flawless-white-toothpolish-75ml/11542528.html" class="bTrack" target="_blank" data-track="Lasting Flawless White : Look Fantastic" data-type="buy-now"><img class="logo-image" src="/img/buy-now/look-fantastic.svg"></a></div> --}}
                <div class="logo-wrap"><a href="https://www.amazon.co.uk/Pearl-Drops-Lasting-Flawless-White/dp/B017NR0XOK/ref=sr_1_6_a_it?ie=UTF8&amp;qid=1521219918&amp;sr=8-6&amp;keywords=pearl+drops" class="bTrack" target="_blank" data-track="Lasting Flawless White : Amazon" data-type="buy-now"><img class="logo-image" src="/img/amazon-logo.png"></a></div>
                <div class="logo-wrap"><a href="https://www.ocado.com/products/pearl-drops-flawless-white-493969011" class="bTrack" target="_blank" data-track="Lasting Flawless White : ocado" data-type="buy-now"><img class="logo-image mh" src="/img/ocado-logo-35pxtall.png"></a></div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="container collection-item-container pink-border" id="instant-natural-white">
        <div class="container collection-inner">
            <div class="row">
                <div class="col-md-6">
                    <img src="/img/collection/instant-natural-white.png" class="collection-product" alt="Instant Natural White">
                </div>
                <div class="col-md-6">
                    <h2 class="label-heading pb-4">Daily Whitening Toothpastes</h2>
                    <h2 class="collection-item-title pb-4">Experience charcoal innovation</br><span class="light-text">With instant natural white</span></h2>
                    <p>Visibly boost the whiteness of your teeth instantly with Pearl Drops Instant Natural White. The clinically proven formula, containing Activated Charcoal, helps to draw out and lift impurities from your teeth &amp; protect against further stains. Optical Brighteners provide an instant whitening effect and the combination of these two key ingredients gives it its distinctive purple colour!</p>
                    <p><strong>Instant and long term&nbsp;whitening*</strong></p>
                    <div class="review-container pb-5">
                        
                        <a href="http://www.boots.com/pearl-drops-instant-natural-white-charcoal-toothpaste-75ml-10227303#BVRRContainer" target="_blank" rel="nofollow" data-track="instant-natural-white" data-type="reviews" class="underline-link bTrack">Read reviews</a>
                    </div>
                    <div class="collection-social-container w-100" data-ref="instant-natural-white">
                        <div class="share-label d-inline-block mt-4 mr-3">Share</div>
                        <a href="#" class="collection-social-link d-inline bTweet"><img src="/img/twitter-square.png" alt="Twitter Logo"></a>
                        
                        <a href="#" class="collection-social-link d-inline bShareFB"><img src="/img/fb-square.png" alt="Facebook Logo"></a>
                    </div>
                    <a href="/pdf/Pearl-Drops-Instant-Natural-White.pdf" target="_blank" class="underline-link w-100 d-block mt-4">Product info</a>
                    <div class="buy-group">
                        <a href="#" class="buy-expand ghost-button">Buy now</a>
                        <div class="popup-menu-wrap">
                            <div class="popup-menu">
                                <div class="logo-wrap"><a href="http://www.boots.com/pearl-drops-instant-natural-white-charcoal-toothpaste-75ml-10227303" class="bTrack" target="_blank" data-track="Instant Natural White : Boots" data-type="buy-now"><img class="logo-image mh" src="/img/boots-logo.png"></a></div>
                <div class="logo-wrap"><a href="https://www.superdrug.com/Toiletries/Dental/Whitening-Toothpaste/Pearl-Drops-Instant-Natural-White-75ml/p/728740" class="bTrack" target="_blank" data-track="Instant Natural White : SuperDrug" data-type="buy-now"><img class="logo-image" src="/img/superdrug-logo.png"></a></div>
                <div class="logo-wrap"><a href="https://www.tesco.com/groceries/en-GB/products/295350122" class="bTrack" target="_blank" data-track="Instant Natural White : Tesco" data-type="buy-now"><img class="logo-image" src="/img/tesco-logo.png"></a></div>
                <div class="logo-wrap"><a href="https://www.sainsburys.co.uk/shop/ProductDisplay?storeId=10151&amp;productId=1191478&amp;urlRequestType=Base&amp;catalogId=10122&amp;langId=44" class="bTrack" target="_blank" data-track="Instant Natural White : Sainsburys" data-type="buy-now"><img class="logo-image" src="/img/sainsburys-logo.png"></a></div>
                {{-- <div class="logo-wrap"><a href="https://www.lookfantastic.com/pearl-drops-instant-natural-white-charcoal-toothpolish-75ml/11542529.html" class="bTrack" target="_blank" data-track="Instant Natural White : Look Fantastic" data-type="buy-now"><img class="logo-image" src="/img/buy-now/look-fantastic.svg"></a></div> --}}
                <div class="logo-wrap"><a href="https://www.amazon.co.uk/Pearl-Drops-Instant-Natural-Toothpaste/dp/B0745KR8SX/ref=sr_1_1_a_it?ie=UTF8&amp;qid=1521219918&amp;sr=8-1&amp;keywords=pearl+drops" class="bTrack" target="_blank" data-track="Instant Natural White : Amazon" data-type="buy-now"><img class="logo-image" src="/img/amazon-logo.png"></a></div>
                <div class="logo-wrap"><a href="https://groceries.asda.com/product/whitening-toothpaste/pearl-drops-instant-natural-white/910002992558" class="bTrack" target="_blank" data-track="Instant Natural White : ASDA" data-type="buy-now"><img class="logo-image" src="/img/asda-logo.png"></a></div>
                <div class="logo-wrap"><a href="https://www.ocado.com/products/pearl-drops-instant-natural-white-493970011" class="bTrack" target="_blank" data-track="Instant Natural White : ocado" data-type="buy-now"><img class="logo-image mh" src="/img/ocado-logo-35pxtall.png"></a></div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="container collection-item-container purple-border" id="luminous-bright-white">
        <div class="container collection-inner">
            <div class="row">
                <div class="col-md-6">
                    <img src="/img/collection/luminous-bright-white.png" class="collection-product" alt="Luminous Bright White">
                </div>
                <div class="col-md-6">
                    <h2 class="label-heading pb-4">Daily Whitening Toothpastes</h2>
                    <h2 class="collection-item-title pb-4">GET AN INSTANT WHITENING LIFT </br><span class="light-text">WITH LUMINOUS BRIGHT WHITE</span></h2>
                    <p>Pearl Drops Luminous Bright White contains a powerful pink formula, featuring our Pro-Shine complex for an instant optical whitening effect and added gloss. Use daily for  an instant whitening effect as well as boosting the whiteness of your teeth by up to 2 shades in&nbsp;1&nbsp;week.</p>
                    <p><strong>Teeth will look instantly whiter*</strong></p>
                    <div class="review-container pb-5">
                        
                        <a href="http://www.boots.com/pearl-drops-luminous-bright-white-toothpaste-75ml-10241093#BVRRContainer" target="_blank" rel="nofollow" data-track="luminous-bright-white" data-type="reviews" class="underline-link bTrack">Read reviews</a>
                    </div>
                    <div class="collection-social-container w-100" data-ref="luminous-bright-white">
                        <div class="share-label d-inline-block mt-4 mr-3">Share</div>
                        <a href="#" class="collection-social-link d-inline bTweet"><img src="/img/twitter-square.png" alt="Twitter Logo">    </a>
                        
                        <a href="#" class="collection-social-link d-inline bShareFB"><img src="/img/fb-square.png" alt="Facebook Logo"></a>
                    </div>
                    <a href="/pdf/Pearl-Drops-Luminous-Bright-White.pdf" target="_blank" class="underline-link w-100 d-block mt-4">Product info</a>
                    <div class="buy-group">
                        <a href="#" class="buy-expand ghost-button">Buy now</a>
                        <div class="popup-menu-wrap">
                            <div class="popup-menu">
                                <div class="logo-wrap"><a href="http://www.boots.com/pearl-drops-luminous-bright-white-toothpaste-75ml-10241093" class="bTrack" target="_blank" data-track="Luminous Bright White : Boots" data-type="buy-now"><img class="logo-image mh" src="/img/boots-logo.png"></a></div>
                <div class="logo-wrap"><a href="https://www.superdrug.com/Toiletries/Dental/Whitening-Toothpaste/Pearl-Drops-Luminous-Bright-White-Toothpolish-75ml/p/754363" class="bTrack" target="_blank" data-track="Luminous Bright White : SuperDrug" data-type="buy-now"><img class="logo-image" src="/img/superdrug-logo.png"></a></div>
                <div class="logo-wrap"><a href="https://www.sainsburys.co.uk/shop/ProductDisplay?storeId=10151&amp;productId=1191480&amp;urlRequestType=Base&amp;catalogId=10122&amp;langId=44" class="bTrack" target="_blank" data-track="Luminous Bright White : Sainsburys" data-type="buy-now"><img class="logo-image" src="/img/sainsburys-logo.png"></a></div>
                <div class="logo-wrap"><a href="https://www.ocado.com/products/pearl-drops-luminous-bright-493971011" class="bTrack" target="_blank" data-track="Luminous Bright White : ocado" data-type="buy-now"><img class="logo-image mh" src="/img/ocado-logo-35pxtall.png"></a></div>
                                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="container collection-item-container grey-border" id="strong-polished-white">
        <div class="container collection-inner">
            <div class="row">
                <div class="col-md-6">
                    <img src="/img/collection/strong-polished-white.png" class="collection-product" alt="Strong Polished White">
                </div>
                <div class="col-md-6">
                    <h2 class="label-heading pb-4">Daily Whitening Toothpastes</h2>
                    <h2 class="collection-item-title pb-4">Four shades whiter teeth</br><span class="light-text">With strong polished white</span></h2>
                    <p>Pearl Drops Strong Polished White combines our advanced whitening formulation with Perlite, a professional dentist-grade ingredient, to give you a smooth, polished feeling, enamel strengthening as well as achieving up to 4 shades whiter teeth when used&nbsp;daily.</p>
                    <p><strong>Enamel strengthening</strong></p>
                    <div class="review-container pb-5">
                        
                        <a href="http://www.boots.com/pearl-drops-strong-polished-white-toothpaste-75ml-10241092#BVRRContainer" target="_blank" rel="nofollow" data-track="strong-polished-white" data-type="reviews" class="underline-link bTrack">Read reviews</a>
                    </div>
                    <div class="collection-social-container w-100" data-ref="strong-polished-white">
                        <div class="share-label d-inline-block mt-4 mr-3">Share</div>
                        <a href="#" class="collection-social-link d-inline bTweet"><img src="/img/twitter-square.png" alt="Twitter Logo">    </a>
                        
                        <a href="#" class="collection-social-link d-inline bShareFB"><img src="/img/fb-square.png" alt="Facebook Logo"></a>
                    </div>
                    <a href="/pdf/Pearl-Drops-Strong-Polished-White.pdf" target="_blank" class="underline-link w-100 d-block mt-4">Product info</a>
                    <div class="buy-group">
                        <a href="#" class="buy-expand ghost-button">Buy now</a>
                        <div class="popup-menu-wrap">
                            <div class="popup-menu">
                                <div class="logo-wrap"><a href="http://www.boots.com/pearl-drops-strong-polished-white-toothpaste-75ml-10241092" class="bTrack" target="_blank" data-track="Strong Polished White : Boots" data-type="buy-now"><img class="logo-image mh" src="/img/boots-logo.png"></a></div>
                <div class="logo-wrap"><a href="https://www.superdrug.com/Toiletries/Dental/Whitening-Toothpaste/Pearl-Drops-Strong-Polished-White-Toothpolish-75ml/p/754361" class="bTrack" target="_blank" data-track="Strong Polished White : SuperDrug" data-type="buy-now"><img class="logo-image" src="/img/superdrug-logo.png"></a></div>
                <div class="logo-wrap"><a href="https://www.amazon.co.uk/Pearl-Drops-Strong-Polished-Toothpolish/dp/B07DWDF6KH" class="bTrack" target="_blank" data-track="Strong Polished White : amazon" data-type="buy-now"><img class="logo-image" src="/img/amazon-logo.png"></a></div>
                                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="container collection-item-container grey-border" id="professional-whitening-treatment">
        <div class="container collection-inner">
            <div class="row">
                <div class="col-md-6">
                    <img src="/img/collection/professional-whitening-treatment.png" class="collection-product" alt="Professional Whitening Treatment">
                </div>
                <div class="col-md-6">
                    <h2 class="label-heading pb-4">Whitening Treatments</h2>
                    <h2 class="collection-item-title pb-4">Restore the natural white of your teeth</br><span class="light-text">WITH OUR PROFESSIONAL WHITENING TREATMENT</span></h2>
                    <p>Inspired by salons and professionals, our Professional Whitening Treatment is your very own three-step whitening treatment that you can use at home, specifically designed to target both surface stains and internal discolouration. This easy to use double-action treatment gets to work straightaway, giving you whiter teeth inside and out – without the need for messy trays, strips or&nbsp;capsules.</p>
                    <p><strong>100% of women achieved 5 shades whiter or more in 10 days*.</strong></p>
                    <div class="review-container pb-5">
                        
                        <a href="http://www.boots.com/beauty/teeth-whitening/whitening-strips-kits/pearl-drops-pure-white-whitening-kit-10176046#BVRRContainer" target="_blank" rel="nofollow" data-track="professional-whitening-treatment" data-type="reviews" class="underline-link bTrack">Read reviews</a>
                    </div>
                    <div class="collection-social-container w-100" data-ref="professional-whitening-treatment">
                        <div class="share-label d-inline-block mt-4 mr-3">Share</div>
                        <a href="#" class="collection-social-link d-inline bTweet"><img src="/img/twitter-square.png" alt="Twitter Logo">    </a>
                        
                        <a href="#" class="collection-social-link d-inline bShareFB"><img src="/img/fb-square.png" alt="Facebook Logo"></a>
                    </div>
                    <a href="/pdf/Pearl-Drops-Professional-Whitening-Treatment.pdf" target="_blank" class="underline-link w-100 d-block mt-4">Product info</a>
                    <div class="buy-group">
                        <a href="#" class="buy-expand ghost-button">Buy now</a>
                        <div class="popup-menu-wrap">
                            <div class="popup-menu">
                <div class="logo-wrap"><a href="https://www.superdrug.com/Toiletries/Dental/Whitening-Kits/Pearl-Drops-Whitening-Kit/p/479201" class="bTrack" target="_blank" data-track="Professional Whitening Treatment : SuperDrug" data-type="buy-now"><img class="logo-image" src="/img/superdrug-logo.png"></a></div>
                <div class="logo-wrap"><a href="https://www.sainsburys.co.uk/gol-ui/SearchDisplayView?filters[keyword]=pearl%20drops&langId=44&storeId=10151&searchType=2&searchTerm=pearl%20drops" class="bTrack" target="_blank" data-track="Professional Whitening Treatment : Sainsburys" data-type="buy-now"><img class="logo-image" src="/img/sainsburys-logo.png"></a></div>                
                                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{-- check border class: grey-border / blue-border / pink-border / green-border --}}

    <section class="container collection-item-container no-border" id="strong-and-white">
        <div class="container collection-inner">
            <div class="row">
                <div class="col-md-6">
                    <img src="/img/collection/strong-and-white.png" class="collection-product" alt="Strong and White">
                </div>
                <div class="col-md-6">
                    <h2 class="label-heading pb-4">Whitening Treatments</h2>
                    <h2 class="collection-item-title pb-4">SLEEP YOUR WAY TO A STRONGER, WHITER SMILE</br><span class="light-text">WITH STRONG &amp; WHITE OVERNIGHT SERUM</span></h2>
                    <p>Pearl Drops Strong &amp; White Overnight Serum, containing Liquid Calcium&trade; and Vitamin E, works while you sleep to strengthen and repair your surface enamel to boost the natural whiteness of your teeth. Its precision brush makes it a quick and easy step in your night time regime. Combine with Pearl Drops Lasting Flawless White for the ultimate whitening&nbsp;boost.</p>
                    <p><strong>Outstanding Whiteness in 3 days*.</strong></p>
                    <div class="review-container pb-5">
                        
                        <a href="https://www.superdrug.com/Toiletries/Dental/Whitening-Toothpaste/Pearl-Drops-Strong-and-White-Overnight-Serum-15ml/p/754364" target="_blank" rel="nofollow" data-track="strong-and-white" data-type="reviews" class="underline-link bTrack">Read reviews</a>
                    </div>
                    <div class="collection-social-container w-100" data-ref="strong-and-white">
                        <div class="share-label d-inline-block mt-4 mr-3">Share</div>
                        <a href="#" class="collection-social-link d-inline bTweet"><img src="/img/twitter-square.png" alt="Twitter Logo">    </a>
                        
                        <a href="#" class="collection-social-link d-inline bShareFB"><img src="/img/fb-square.png" alt="Facebook Logo"></a>
                    </div>
                    <a href="/pdf/Pearl-Drops-Overnight-Serum.pdf" target="_blank" class="underline-link w-100 d-block mt-4">Product info</a>
                    <div class="buy-group">
                        <a href="#" class="buy-expand ghost-button">Buy now</a>
                        <div class="popup-menu-wrap">
                            <div class="popup-menu">
                                <div class="logo-wrap"><a href="https://www.superdrug.com/Toiletries/Dental/Whitening-Toothpaste/Pearl-Drops-Strong-and-White-Overnight-Serum-15ml/p/754364" class="bTrack" target="_blank" data-track="Strong and White : SuperDrug" data-type="buy-now"><img class="logo-image" src="/img/superdrug-logo.png"></a></div>
                                <div class="logo-wrap"><a href="https://www.sainsburys.co.uk/gol-ui/Product/pearl-drops-strong-and-white-overnight-serum-15ml" class="bTrack" target="_blank" data-track="Strong and White : Sainsburys" data-type="buy-now"><img class="logo-image" src="/img/sainsburys-logo.png"></a></div>                
                                <div class="logo-wrap"><a href="https://www.tesco.com/groceries/en-GB/products/305037104" class="bTrack" target="_blank" data-track="Strong and White : TESCO" data-type="buy-now"><img class="logo-image" src="/img/tesco-logo.png"></a></div>                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{--
    <section class="container collection-item-container  last-collection-item" id="pure-natural-white">
        <div class="container collection-inner">
            <div class="row">
                <div class="col-md-6">
                    <img src="/img/collection/pure-natural-white.png" class="collection-product" alt="Pure Natural White">
                </div>
                <div class="col-md-6">
                    <h2 class="label-heading pb-4">Daily Whitening Toothpastes</h2>
                    <h2 class="collection-item-title pb-4">OUR LOWEST ABRASION FORMULA</br><span class="light-text">WITH PURE NATURAL WHITE</span></h2>
                    <p>Harness the power of bentonite clay with Pearl Drops Pure Natural White, our specially formulated low-abrasion solution to a whiter smile. Packed with charged white bentonite clay and 90% natural origin ingredients†, you will achieve a gradual whitening effect with daily use with the additional benefit of enamel protection.</p>
                    <p><strong>Whiter teeth in&nbsp;2&nbsp;weeks*</strong></p>
                    <div class="review-container pb-5">
                        
                        <a href="http://www.boots.com/pearl-drops-pure-natural-white-toothpolish-75ml-10241091#BVRRContainer" target="_blank" rel="nofollow" data-track="pure-natural-white" data-type="reviews" class="underline-link bTrack">Read reviews</a>
                    </div>
                    <div class="collection-social-container w-100" data-ref="pure-natural-white">
                        <div class="share-label d-inline-block mt-4 mr-3">Share</div>
                        <a href="#" class="collection-social-link d-inline bTweet"><img src="/img/twitter-square.png" alt="Twitter Logo">    </a>
                        
                        <a href="#" class="collection-social-link d-inline bShareFB"><img src="/img/fb-square.png" alt="Facebook Logo"></a>
                    </div>
                    <a href="/pdf/Pearl-Drops-Pure-Natural-White.pdf" target="_blank" class="underline-link w-100 d-block mt-4">Product info</a>

                    <div class="buy-group">
                        <a href="#" class="buy-expand ghost-button">Buy now</a>
                        <div class="popup-menu-wrap">
                            <div class="popup-menu">
                                <div class="logo-wrap"><a href="http://www.boots.com/pearl-drops-pure-natural-white-toothpolish-75ml-10241091" class="bTrack" target="_blank" data-track="Pure Natural White : Boots" data-type="buy-now"><img class="logo-image mh" src="/img/boots-logo.png"></a></div>
                <div class="logo-wrap"><a href="https://www.superdrug.com/Toiletries/Dental/Whitening-Toothpaste/Pearl-Drops-Pure-Natural-White-Toothpolish-75ml/p/754362" class="bTrack" target="_blank" data-track="Pure Natural White : SuperDrug" data-type="buy-now"><img class="logo-image" src="/img/superdrug-logo.png"></a></div>
                        <div class="logo-wrap"><a href="https://www.lookfantastic.com/pearl-drops-pure-natural-white-75ml/11542526.html" class="bTrack" target="_blank" data-track="Pure Natural White : Look Fantastic" data-type="buy-now"><img class="logo-image" src="/img/buy-now/look-fantastic.svg"></a></div>
                                    </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    --}}

    @include('main.layouts.partials._social')
@endsection

@section('components')
	
@endsection
