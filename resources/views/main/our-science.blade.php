@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<!--Hero section-->
    <section class="hero science-hero">
        <div class="hero-content-container">
            <div class="white-bg-container">
                <h1>This is our science</h1>
                <p><strong>Real whitening solutions that really work.</strong></br>Innovation is the beating heart of Pearl Drops. From day one, our passion to better ourselves has been the driving force behind everything we do. It’s what gets us out of bed every morning. (And, occasionally, what keeps us awake at&nbsp;night).</p>
            </div>
        </div>
    </section>
    <section class="jumbotron jumbotron-fluid blue-full-width-container">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="xl-heading text-center pb-5">OUR LATEST INDEPENDENT TEST PROVES WE ARE&nbsp;THE&nbsp;BEST</h2>
                </div>
                <div class="col-lg-6 chart-parts">
                    <img src="img/pearl-drops-independent-test.png" class="pb-2" alt="Independent Test Results">
                    <p class="note">*In-vitro testing data October 2017</p>
                </div>
                <div class="col-lg-6 chart-parts">
                    <p class="large-p">We beat the rest in test after test. We put our formulas to the test against our leading competitors. We tested for two things; abrasion levels (RDA) and cleaning/whitening performance (PCR), with the ideal being low abrasion with highly effective whitening. The results speak for themselves, taking the top spot for performance whilst remaining well within the low abrasion parameters – a testament to our 50 years of science and&nbsp;innovation.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="panel-grid-container panel-grid-container-desktop panel-grid-science container">
        <h2 class="xl-heading text-center pb-5 pt-4">Breakthroughs and discoveries</h2>
        <div class="row">
            <div class="col-lg-4 col-md-6 col-12 panel-item science-panel-1">
                <h3>Activated Charcoal</h3>
                <hr>
                <p class="quote">A natural ingredient with a long history of detoxing and cleansing&nbsp;benefits.</p>
                <p class="text-center">Activated Charcoal is naturally charged and has been used over the years to help detox by drawing out and lifting impurities. This powerful yet gentle ingredient has been combined with our whitening formula, delivering a gradual whitening effect, whilst being kind to your&nbsp;teeth.</p>
                <p class="text-center"><a href="{{ route('collection') }}#{{ __('products.product-2') }}" class="ghost-button ghost-button-white">View the product</a></p>
            </div>
            <div class="col-lg-4 col-md-6 col-12 panel-item image-panel science-panel-2"></div>
            <div class="col-lg-4 col-md-6 col-12 panel-item science-panel-3">
                <h3>Liquid Calcium</h3>
                <hr>
                <p class="quote">Liquid Calcium is a Mineral naturally present in the mouth that helps teeth repair themselves.</p>
                <p class="text-center">An uneven enamel surface causes teeth to appear dull and discoloured. This is where Liquid Calcium&trade; steps in. The ingredients fill the micro-cracks and crevices that result from everyday wear and tear – helping smooth surface enamel and return teeth to their natural glow and adding&nbsp;shine.</p>
                <p class="text-center"><a href="{{ route('collection') }}#{{ __('products.product-1') }}" class="ghost-button ghost-button-white">View the product</a></p>
            </div>

            <div class="col-lg-4 col-md-6 col-12 panel-item science-panel-4">
                <h3>Perlite</h3>
                <hr>
                <p class="quote">A natural tooth polisher used by&nbsp;dentists.</p>
                <p class="text-center">Perlite has long since been used in the paste used by  dentists and hygienists when giving a professional clean, as it is known for its high polishing performance and ability to remove surface stains. We have harnessed this power to give you a professional dentist-clean feeling at&nbsp;home.</p>
                <p class="text-center"><a href="{{ route('collection') }}#{{ __('products.product-5') }}" class="ghost-button ghost-button-white">View the product</a></p>
            </div>
            <div class="col-lg-4 col-md-6 col-12 panel-item science-panel-5">
                <h3>Fluoride and Plaque Removers</h3>
                <hr>
                <p class="quote">Polishes and protects&nbsp;teeth.</p>
                <p class="text-center">All our formulas feature essential fluoride to help strengthen enamel and protect against tooth decay, making them perfect for daily use. Our low abrasion polishing agents and plaque removers also help to remove surface plaque for a glossy and bright finish with regular&nbsp;brushing.</p>
                <p class="text-center">Found in all our Specialist Whitening toothpastes.</p>
            </div>
            <div class="col-lg-4 col-md-6 col-12 panel-item image-panel science-panel-6 d-none d-lg-flex"></div>

            <div class="col-lg-4 col-md-6 col-12 panel-item image-panel science-panel-7"></div>
            <div class="col-lg-4 col-md-6 col-12 panel-item science-panel-8">
                <h3>Refreshing Mint</h3>
                <hr>
                <p class="quote">A refreshing lift to start and finish your&nbsp;day.</p>
                <p class="text-center">Our range of formulas are infused with a range of different mint flavours, including peppermint and essential oils and even a slow release technology that makes sure freshness lasts long after&nbsp;brushing.</p>
                <p class="text-center">Found in all our Specialist Whitening toothpastes.</p>
            </div>
            <div class="col-lg-4 col-md-6 col-12 panel-item science-panel-9">
                <h3>White Clay Extract</h3>
                <hr>
                <p class="quote">A gentle way to lift&nbsp;impurities.</p>
                <p class="text-center">Another wonder ingredient used for years in beauty and skincare to help draw out impurities. When it becomes hydrated, the negative ions are activated and begin their work of attracting free radicals, impurities and other toxins to them. This amazing charged Bentonite Clay is a naturally occurring mineral created by volcanic&nbsp;ash.</p>
                <p class="text-center"><a href="{{ route('collection') }}#{{ __('products.product-3') }}" class="ghost-button ghost-button-white">View the product</a></p>
            </div>
        </div>
    </section>

    @include('main.layouts.partials._social')
@endsection

@section('components')
	
@endsection