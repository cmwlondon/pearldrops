@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<!--Hero section-->
    <section class="page-intro container">
        <div class="row">
            <div class="col-md-6">
                <h1>Contact Us</h1>
            </div>
        </div>
    </section>
    <!-- Start contact block-->
    <section class="container">
        <div class="row">
            <div class="col-md-12 form">
                <div class="intro tal">
                    <p class="col1">Got a question about Pearl Drops products? Get in touch via our contact form below or call the Pearl Drops team on 0800 121 6080:</p>
                    <p class="col2">For press enquiries contact Capsule Communications Ltd<br/>Tel: 0207 580 4312<br/>Email. <a href="mailto:hello@capsulecomms.com">hello@capsulecomms.com</a></p>
                </div>

                @if (session('response'))
                    <p class="response">{{ session('response.message') }}</p>
                @else


                {!! Form::open(array('url' => [route('contact')], 'method' => 'POST', 'id' => 'supportForm') )!!}

                <div class="formRow">
                    {!! Form::label('title', 'Title: *') !!}
                    {!! Form::select('title', $titlesArr, null, ['id' => 'title', 'class' => 'input short'] ) !!}
                    {!! $errors->first('title', '<small class="error">:message</small>') !!}
                </div>

                <div class="formRow">
                    {!! Form::label('firstname', 'First Name: *') !!}
                    {!! Form::text('firstname',null,['placeholder' => '', 'id' => 'firstname', 'class' => 'input']) !!}
                    {!! $errors->first('firstname', '<small class="error">:message</small>') !!}
                </div>

                <div class="formRow">
                    {!! Form::label('lastname', 'Last Name: *') !!}
                    {!! Form::text('lastname',null,['placeholder' => '', 'id' => 'lastname', 'class' => 'input']) !!}
                    {!! $errors->first('lastname', '<small class="error">:message</small>') !!}
                </div>

                {{-- <div class="formRow">
                    {!! Form::label('company', 'Company:') !!}
                    {!! Form::text('company',null,['placeholder' => '', 'id' => 'company', 'class' => 'input']) !!}
                    {!! $errors->first('company', '<small class="error">:message</small>') !!}
                </div> --}}

                {{-- <div class="formRow">
                    {!! Form::label('address', 'Address:') !!}
                    {!! Form::text('address',null,['placeholder' => '', 'id' => 'address', 'class' => 'input']) !!}
                    {!! $errors->first('address', '<small class="error">:message</small>') !!}
                </div> --}}

                {{-- <div class="formRow">
                    {!! Form::label('city', 'City:') !!}
                    {!! Form::text('city',null,['placeholder' => '', 'id' => 'city', 'class' => 'input']) !!}
                    {!! $errors->first('city', '<small class="error">:message</small>') !!}
                </div> --}}

                {{--<div class="formRow">
                    {!! Form::label('county', 'County:') !!}
                    {!! Form::text('county',null,['placeholder' => '', 'id' => 'county', 'class' => 'input']) !!}
                    {!! $errors->first('county', '<small class="error">:message</small>') !!}
                </div>--}}

                {{-- <div class="formRow">
                    {!! Form::label('postcode', 'Postcode:') !!}
                    {!! Form::text('postcode',null,['placeholder' => '', 'id' => 'postcode', 'class' => 'input']) !!}
                    {!! $errors->first('postcode', '<small class="error">:message</small>') !!}
                </div> --}}

                {{-- <div class="formRow">
                    {!! Form::label('country', 'Country:') !!}
                    {!! Form::text('country',null,['placeholder' => '', 'id' => 'country', 'class' => 'input']) !!}
                    {!! $errors->first('country', '<small class="error">:message</small>') !!}
                </div> --}}

                <div class="formRow">
                    {!! Form::label('phone', 'Phone Number:') !!}
                    {!! Form::text('phone',null,['placeholder' => '', 'id' => 'phone', 'class' => 'input']) !!}
                    {!! $errors->first('phone', '<small class="error">:message</small>') !!}
                </div>

                <div class="formRow">
                    {!! Form::label('email', 'Email Address: *') !!}
                    {!! Form::text('email',null,['placeholder' => '', 'id' => 'email', 'class' => 'input']) !!}
                    {!! $errors->first('email', '<small class="error">:message</small>') !!}
                </div>

                <div class="formRow">
                    {!! Form::label('product', 'Product:') !!}
                    {!! Form::text('product',null,['placeholder' => '', 'id' => 'product', 'class' => 'input']) !!}
                    {!! $errors->first('product', '<small class="error">:message</small>') !!}
                </div>

                <div class="formRow">
                    {!! Form::label('comments', 'Comments:') !!}
                    {!! Form::textarea('comments',null,['placeholder' => '', 'id' => 'comments', 'rows' => '4', 'class' => 'input']) !!}
                    {!! $errors->first('comments', '<small class="error">:message</small>') !!}
                </div>

                <div class="formRow buttonRow">
                    {!! Form::submit('Submit', array('class' => 'btn', 'id' => 'bSubmit', 'onClick' => "ga('send', 'event', { eventCategory: 'Contact', eventAction: 'Submit', eventLabel: 'Contact Form'});")) !!}
                </div>

                {!! Form::close() !!}

                @endif
            </div>
        </div>
    </section>
    <!--End contact block-->

    @include('main.layouts.partials._social')
@endsection

@section('components')
	
@endsection