@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<!--Hero section-->
    <section class="page-intro container">
        <div class="row">
            <div class="col-md-6">
                <h1>Where to buy </br>pearl drops</h1>
            </div>
        </div>
    </section>
    <!--Start where to buy icons -->
   <section class="container">
        <div class="row where-to-buy-row">

            <div class="col-md-4">
                <a href="http://www.boots.com/brands/pearl-drops" class="bTrack" target="_blank" data-track="Boots" data-type="where-to-buy"><img src="/img/boots-logo.png" class="where-to-buy-image left-aligned-logo" alt="Boots"></a>
            </div>
            <div class="col-md-4">
                <a href="https://www.superdrug.com/b/Pearl%20Drops" class="bTrack" target="_blank" data-track="SuperDrug" data-type="where-to-buy"><img src="/img/superdrug-logo.png" class="where-to-buy-image" alt="Superdrug"></a>
            </div>
            <div class="col-md-4">
                <a href="https://www.tesco.com/groceries/en-GB/search?query=pearl%20drops&amp;icid=tescohp_sws-1_pearl%20drops " class="bTrack" target="_blank" data-track="Tesco" data-type="where-to-buy"><img src="/img/tesco-logo.png" class="where-to-buy-image right-aligned-logo" alt="TESCO"></a>
            </div>
            <div class="col-md-4">
                <a href="https://www.sainsburys.co.uk/webapp/wcs/stores/servlet/SearchDisplayView?catalogId=10123&amp;langId=44&amp;storeId=10151&amp;krypto=q1i3Obrs9jocuHEr5gKO3JoekOGUwXutHtBHwDwwlprSX9Ar2u5sIEb2l8pAcGtisAcTgXo%2F7vw0UrDLfru25iy9SteQtIe8pfPtu%2BZvxE%2FLIOHBK0%2BPuyR7VyCzJyD2%2FtsFQ4Xq0qsnEIPxo0ofuJy6Fv4ds%2F8TvNs53ji05jnS8%2BiKL45uNeSgYM9mFUj4#langId=44&amp;storeId=10151&amp;catalogId=10123&amp;categoryId=&amp;parent_category_rn=&amp;top_category=&amp;pageSize=36&amp;orderBy=&amp;searchTerm=pearl%20drops&amp;beginIndex=0&amp;hideFilters=true&amp;categoryFacetId1=" class="bTrack" target="_blank" data-track="Sainsburys" data-type="where-to-buy"><img src="/img/sainsburys-logo.png" class="where-to-buy-image left-aligned-logo" alt="Sainsbury's"></a>
            </div>
            <div class="col-md-4">
                <a href="https://groceries.asda.com/search/pearl%20drops?cmpid=ahc-_-ghs-_-asdacom-_-hp-_-search-pearl-drops" class="bTrack" target="_blank" data-track="ASDA" data-type="where-to-buy"><img src="/img/asda-logo.png" class="where-to-buy-image" alt="ASDA"></a>
            </div>
            <div class="col-md-4">
                <a href="https://www.amazon.co.uk/s?k=pearl+drops+toothpaste&ref=nb_sb_noss_2" class="bTrack" target="_blank" data-track="Amazon" data-type="where-to-buy"><img src="/img/amazon-logo.png" class="where-to-buy-image left-aligned-logo" alt="Amazon"></a>
            </div>

            <div class="col-md-4">
                <a href="https://www.ocado.com/search?entry=pearl%20drops" class="bTrack" target="_blank" data-track="OCADO" data-type="where-to-buy"><img src="/img/ocado-logo2.png" class="where-to-buy-image right-aligned-logo" alt="OCADO"></a>
            </div>

        </div>
    </section>
    <!--End where to buy icons-->

    @include('main.layouts.partials._social')
@endsection

@section('components')
	
@endsection