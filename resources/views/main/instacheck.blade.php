@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<!--Hero section-->
    <section class="page-intro container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Instacheck</h1>
            </div>
        </div>
    </section>
    <!--Start content -->
    <section class="container">
        <div class="row">
            <div class="col-sm-12">

                <ul>
                @foreach ($data as $item)
                <li>
                    <p>id: {{ $item->id }} type: {{ $item->type }}</p>
                    <p>{!! $item->caption->text !!}</p>
                    <a href="{{ $item->link }}" target="-blank">View on instagram</a><br>
                    <img alt="{{ $item->id }}" src="{{ $item->images->thumbnail->url }}">
                    @if ( array_key_exists('videos', $item) && $item->type === 'video' )
                        <video preload="metadata" controls loop playsinline>
                        <source src="{{ $item->videos->standard_resolution->url }}" type="video/mp4">
                        </video>
                    @endif
                    
                </li>
                @endforeach
                </ul>

            </div>
        </div>
    </section>
    <!--End content-->

    @include('main.layouts.partials._social')
@endsection

@section('components')
	
@endsection