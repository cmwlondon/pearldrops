@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<!--Hero section-->
    <section class="page-intro container">
        <div class="row">
            <div class="col-md-6">
                <h1>Cookie Notice</h1>
            </div>
        </div>
    </section>
    <!--Start content -->
    <section class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2>Cookies</h2>
                <p>We have outlined below the types of cookie that are used on this Site.</p>
                {{--
                <p>Cookies are small text files that are placed on your computer by a website in order to make websites work, or work more efficiently, as well as to provide information to the owners of the site. Cookies help us to run the Site and improve its interactivity and our services.</p>
                <p>We have outlined below the types of cookie that are used on this Site.</p>
                <h2>Strictly necessary cookies</h2>
                <p>We use first-party cookies to help the Site work in the best possible manner. You may opt out of receiving these cookies and delete them using your internet browser settings. However, your user experience may be impacted.</p>
                <table cellpadding="0" cellspacing="0" border="1" class="cookie-table">
                    <tbody>
                        <tr>
                            <th width="120">Name</th>
                            <th>Purpose</th>
                            <th width="120">Retention</th>
                        </tr>
                        <tr>
                            <td>XSRF-TOKEN</td>
                            <td>Used to ensure data transmitted from forms back to the server is not intercepted by third parties and manipulated on route.</td>
                            <td class="tac">2 hours</td>
                        </tr>
                        <tr>
                            <td>laravel_session</td>
                            <td>Allows the website to communicate with itself as users navigate between pages, holding temporary information such as form error messages.</td>
                            <td class="tac">2 hours</td>
                        </tr>
                    </tbody>
                </table>
                <h2>Analytics cookies</h2>
                <p>We use Google Analytics cookies to analyse how this Site is used. Google analytics cookies monitor how visitors navigate the Site and how they found their way here. We use these so that we can see the total amount of visitors to the Site; this does not offer individual information. It also allows us to see what content our users enjoy most which helps us to provide you with the best service. You may at any time change your cookie settings to accept or delete these cookies by <a href="https://tools.google.com/dlpage/gaoptout" target="_blank">clicking here</a>.</p>
                <p>Some assets such as the code required for the video player are loaded from a content delivery network, and this service uses a cookie to help with caching assets for users.</p>
                <p>&nbsp;</p>
                 --}}

<!-- OneTrust Cookies List start -->
<div id="ot-sdk-cookie-policy">Cookie Notice</div>
<!-- OneTrust Cookies List end -->
            </div>
        </div>
    </section>
    <!--End content-->

    @include('main.layouts.partials._social')
@endsection

@section('components')
	
@endsection