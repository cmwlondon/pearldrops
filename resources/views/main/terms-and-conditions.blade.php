@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<!--Hero section-->
    <section class="page-intro container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Terms &amp; Conditions</h1>
            </div>
        </div>
    </section>
    <!--Start content -->
    <section class="container">
        <div class="row">
            <div class="col-sm-12">
                <p>All information ("Information") which you read or see at this Site is protected by copyright and/or other intellectual property laws. The contents ("Contents") are owned by Church &amp; Dwight UK Limited ("Church &amp; Dwight"), its affiliates, or other third parties from whom Church &amp; Dwight has received certain legal rights. You may not report, modify, publish, sell, reproduce, distribute, post, display, transmit, or in any way exploit any of this Site's Contents for commercial purposes. You may, if you wish, download and retain on a disk or in hard drive form a single copy of the Contents of this Site for personal, non-commercial purposes as long as you do not remove any proprietary notices.</p>
                <p>While Church &amp; Dwight has made reasonable efforts to include Information at this site which is accurate and timely, Church &amp; Dwight makes no representations or warranties as to the accuracy of such Information and, specifically, Church &amp; Dwight assumes no liability or responsibility for any errors or omissions in the Information or the Contents of this Site. Moreover, Church &amp; Dwight neither warrants nor represents that your use of the Information will not infringe the rights of third parties who are not affiliated with Church &amp; Dwight. Your access to and use of this Site are at your own risk, and neither Church &amp; Dwight nor any party involved in the creation, transmittal, or maintenance of this Site shall be liable to you for any direct, indirect, consequential, incidental or punitive damages of any kind allegedly arising out of your access or use of this Site, or your inability to access or use this Site. Notwithstanding anything to the contrary contained herein, the Contents of this Site are provided to you on an "AS IS" basis and specifically WITHOUT WARRANTY OF ANY KIND, WHETHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, SATISFACTORY QUALITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. Please note that some jurisdictions may not permit the exclusion of implied warranties and, as a result, some of the exclusions referenced above may not apply to you. You should check your local laws for any limitations or restrictions which might impact you.</p>
                <p>Church &amp; Dwight assumes no responsibility and shall not be liable for any damages to, or any viruses that may infect, your computer equipment resulting from your access to or use of this Site, or the downloading of any Information from this Site.</p>
                <p>The trademarks, logos and service marks ("Trademarks") displayed throughout the Site are registered and unregistered Trademarks of Church &amp; Dwight and/or third party licensors. No license, right or permission is granted to you for any use of the Trademarks by you or anyone authorized by you. Misuse of the Trademarks is strictly prohibited and Church &amp; Dwight will aggressively enforce its intellectual property rights to the fullest extent of the law, including the pursuit of criminal prosecution whenever and wherever necessary.</p>
                <p>The pictures and images of people, products, places or things displayed on this Site are either the property of Church &amp; Dwight or are used with the permission of third parties. Any use of such pictures or images by you or anyone authorized or affiliated with you is strictly prohibited. Unauthorized uses of pictures and images may violate copyright or trademark laws, privacy laws, or communication laws or regulations.</p>
                <p>Descriptions of Church &amp; Dwight's products contained within the Site shall not constitute product labeling. You should use Church &amp; Dwight's products in accordance with the instructions contained on the cartons and labels found on those products in the country of purchase.</p>
                <p>Internet users located in countries which are subject to U.S.A. trade embargo laws and regulations are prohibited from accessing this Site and are asked to promptly exit at this time.</p>
                <p>Any communication or material transmitted to this Site by electronic mail or other means, shall be treated as non-confidential and non-proprietary. This includes ideas, suggestions, comments, questions and any other information or data. Anything submitted to Church &amp; Dwight can be used, reproduced, transmitted, disclosed or published by Church &amp; Dwight or its affiliates without restriction or compensation, except we would like you to be aware that Church &amp; Dwight has established an Internet Privacy Policy regarding personal data and other information you transmit to this Site.</p>
                <p>Church &amp; Dwight has not reviewed all of the sites which are linked to this Site. As a result, Church &amp; Dwight is not responsible for the content of such linked sites and your linking to such sites is at your own risk.</p>
                <p>Church &amp; Dwight reserves the right to alter or delete any material from the Content of this Site at any time. Church &amp; Dwight further reserves the right to discontinue this Site at any time and without notice.</p>
                <p>This Agreement shall be governed by and construed in accordance with the laws of England, U.K., without regard to any conflicts of law provisions. Any cause of action with respect to this Site or this Agreement must be filed in courts of competent jurisdiction in England within one year after the cause of action has accrued; unless such a filing is made in accordance with such rules, the cause shall be permanently barred.</p>
                <p>&nbsp;</p>
            </div>
        </div>
    </section>
    <!--End content-->

    @include('main.layouts.partials._social')
@endsection

@section('components')
	
@endsection