@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<!--Hero section-->
    <section class="page-intro container">
        <div class="row">
            <div class="col-sm-12">
                <h1>THIRD PARTY INFORMATION COLLECTION</h1>
            </div>
        </div>
    </section>
    <!--Start content -->
    <section class="container">
        <div class="row">
            <div class="col-sm-12">
                <p>NONE</p>
                <p>&nbsp;</p>
            </div>
        </div>
    </section>
    <!--End content-->

    @include('main.layouts.partials._social')
@endsection

@section('components')
	
@endsection