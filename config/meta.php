<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Meta Data All in One Place
    |--------------------------------------------------------------------------
    */

    '/' => [
        'title' => 'Pearl Drops Real Whitening',
        'desc'  => 'Pearl Drops toothpolish - a specialist whitening product for each and every smile.',
        'image' => 'pearl-drops-logo.png',
        'pagetype' => 'website',
        'keywords' => 'pearldrops,toothpaste,whitening,toothpolish,whiteteeth,white,teeth,whiter',
        'keyphrases' => 'pearl drops, white teeth, tooth whitening, teeth whitening, whiter teeth'
    ],
    'our-vision' => [
        'title' => 'The Whitening Vision of Pearl Drops',
        'desc'  => 'Find out what motives Pearl Drops to create specialist whitening solutions that can help every woman to feel special and confident in their every day.',
        'image' => 'pearl-drops-logo.png',
        'pagetype' => 'article',
        'keywords' => 'pearldrops,toothpaste,whitening,toothpolish,whiteteeth,white,teeth,whiter',
        'keyphrases' => 'pearl drops, white teeth, tooth whitening, teeth whitening, whiter teeth'
    ],
    'our-history' => [
        'title' => 'The History of Pearl Drops',
        'desc'  => 'Read about Pearl Drops\' 50 years of experience as teeth whitening specialists.',
        'image' => 'pearl-drops-logo.png',
        'pagetype' => 'article',
        'keywords' => 'pearldrops,toothpaste,whitening,toothpolish,whiteteeth,white,teeth,whiter',
        'keyphrases' => 'pearl drops, white teeth, tooth whitening, teeth whitening, whiter teeth'
    ],

    'our-science' => [
        'title' => 'The Science Behind Pearl Drops',
        'desc'  => 'Pearl Drops creates whitening solutions that really work - read about the science and motivations behind our innovative formulas.',
        'image' => 'pearl-drops-logo.png',
        'pagetype' => 'article',
        'keywords' => 'pearldrops,toothpaste,whitening,toothpolish,science,technology,innovation,whiteteeth,white,teeth,whiter,science',
        'keyphrases' => 'does pearl drops work, tooth whitening science, effective tooth whitening, pearl drops, white teeth, tooth whitening, teeth whitening, whiter teeth, how to whiten your teeth'
    ],

    'our-collection' => [
        'title' => 'The Pearl Drops Collection and Range',
        'desc'  => 'Browse the Pearl Drops range of innovative teeth whitening solutions.',
        'image' => 'pearl-drops-logo.png',
        'pagetype' => 'product.group',
        'keywords' => 'pearldrops,toothpaste,whitening,toothpolish,whiteteeth,white,teeth,whiter,science',
        'keyphrases' => 'effective tooth whitening, pearl drops, white teeth, tooth whitening, whiter teeth'
    ],

    /*
    |--------------------------------------------------------------------------
    | Don't forget to add in the end points for Facebook.
    |--------------------------------------------------------------------------
    */

    'our-collection/lasting-flawless-white' => [
        'title' => 'Pearl Drops Lasting Flawless White',
        'desc'  => 'Remove 100% more surface stains with Pearl Drops Lasting Flawless White. Discover ultimate stain removal with our low abrasion formula.',
        'image' => 'lasting-flawless-white.jpg',
        'pagetype' => 'product',
        'keywords' => 'long-lasting,pearldrops,toothpaste,whitening,toothpolish,whiteteeth,white,teeth,whiter',
        'keyphrases' => 'long lasting tooth whitening, effective tooth whitening, white teeth, tooth whitening, whiter teeth'
    ],
    'our-collection/instant-natural-white' => [
        'title' => 'Pearl Drops Instant Natural White',
        'desc'  => 'Instantly boost the whiteness of your teeth with Pearl Drops Instant Natural White. Activated Charcoal helps lift impurities from your teeth.',
        'image' => 'instant-natural-white.jpg',
        'pagetype' => 'product',
        'keywords' => 'long-lasting,instant,pearldrops,toothpaste,whitening,toothpolish,whiteteeth,white,teeth,whiter',
        'keyphrases' => 'effective tooth whitening, whiter teeth, tooth whitening, instant tooth whitening, charcoal toothpaste'
    ],
    'our-collection/pure-natural-white' => [
        'title' => 'Pearl Drops Pure Natural White',
        'desc'  => 'Harness the power of bentonite clay with New Pearl Drops Pure Natural White. Discover our lowest abrasion formula.',
        'image' => 'pure-natural-white.jpg',
        'pagetype' => 'product',
        'keywords' => 'long-lasting,pearldrops,toothpaste,whitening,toothpolish,whiteteeth,white,teeth,whiter',
        'keyphrases' => 'effective tooth whitening, whiter teeth, teeth whitening, whitening toothpaste'
    ],
    'our-collection/luminous-bright-white' => [
        'title' => 'Pearl Drops Luminous Bright White',
        'desc'  => 'Get an instant whitening lift with our powerful pink formula. New Pearl Drops Luminous Bright White gives an instant whitening effect.',
        'image' => 'luminous-bright-white.jpg',
        'pagetype' => 'product',
        'keywords' => 'long-lasting,pearldrops,toothpaste,whitening,toothpolish,whiteteeth,white,teeth,whiter',
        'keyphrases' => 'effective tooth whitening, whiter teeth, teeth whitening, whitening toothpaste, pink toothpaste'
    ],
    'our-collection/strong-polished-white' => [
        'title' => 'Pearl Drops Strong Polished White',
        'desc'  => 'Get up to four shades whiter teeth with New Pearl Drops Strong Polished White. Professional grade Perlite strengthens enamel.',
        'image' => 'strong-polished-white.jpg',
        'pagetype' => 'product',
        'keywords' => 'long-lasting,pearldrops,toothpaste,whitening,toothpolish,whiteteeth,white,teeth,whiter',
        'keyphrases' => 'effective tooth whitening, whiter teeth, teeth whitening, whitening toothpaste'
    ],
    'our-collection/professional-whitening-treatment' => [
        'title' => 'Pearl Drops Professional Whitening Treatment',
        'desc'  => 'Restore the natural whiteness of your teeth with the Pearl Drops 3-step Professional Whitening Treatment.',
        'image' => 'professional-whitening-treatment.jpg',
        'pagetype' => 'product',
        'keywords' => 'long-lasting,pearldrops,toothpaste,whitening,toothpolish,whiteteeth,white,teeth,whiter',
        'keyphrases' => 'effective tooth whitening, whiter teeth, teeth whitening, whitening toothpaste, '
    ],
    'our-collection/strong-and-white' => [
        'title' => 'Pearl Drops Strong and White',
        'desc'  => 'Sleep your way to a stronger, whiter smile with Pearl Drops Strong & White Overnight Serum.',
        'image' => 'strong-and-white.jpg',
        'pagetype' => 'product',
        'keywords' => 'long-lasting,pearldrops,toothpaste,whitening,toothpolish,whiteteeth,white,teeth,whiter',
        'keyphrases' => 'effective tooth whitening, whiter teeth, teeth whitening, whitening toothpaste, '
    ],

    'contact-us' => [
        'title' => 'Contact Pearl Drops',
        'desc'  => 'From questions to comments, get in touch with Pearl Drops here and share your story.',
        'image' => 'pearl-drops-logo.png',
        'pagetype' => 'website',
        'keywords' => 'contact,pearldrops,products,toothpaste,helpline,enquiries,number,questions,help',
        'keyphrases' => 'customer services, consumer care, help line, contact pearl drops, consumer care'
    ],

    'where-to-buy' => [
        'title' => 'Where To Buy Pearl Drops',
        'desc'  => 'Shop the Pearl Drops range of innovative teeth whitening solutions.',
        'image' => 'pearl-drops-logo.png',
        'pagetype' => 'website',
        'keywords' => 'buy,shop,purchase,online,pearldrops,retailer,shops,available',
        'keyphrases' => 'where to buy pearl drops, where to buy whitening toothpaste, online shops, where sells pearl drops'
    ],

    'terms-and-conditions' => [
        'title' => 'Pearl Drops Terms and Conditions',
        'desc'  => 'Read about the Terms and Conditions of visiting our Pearl Drops site.',
        'image' => 'pearl-drops-logo.png',
        'pagetype' => 'website',
        'keywords' => 'pearldrops,toothpaste,whitening,toothpolish,whiteteeth,white,teeth,whiter,terms,conditions,website',
        'keyphrases' => 'pearl drops, white teeth, tooth whitening, teeth whitening, whiter teeth'
    ],

    'cookie-notice' => [
        'title' => 'Pearl Drops Cookie Notice',
        'desc'  => 'Read about the Cookie Notice for the Pearl Drops website.',
        'image' => 'pearl-drops-logo.png',
        'pagetype' => 'website',
        'keywords' => 'pearldrops,toothpaste,whitening,toothpolish,whiteteeth,white,teeth,whiter,cookies,website',
        'keyphrases' => 'pearl drops, white teeth, tooth whitening, teeth whitening, whiter teeth'
    ],

    'privacy-policy' => [
        'title' => 'Pearl Drops Policies',
        'desc'  => 'Read about the Privacy Policy for the Pearl Drops website.',
        'image' => 'pearl-drops-logo.png',
        'pagetype' => 'website',
        'keywords' => 'pearldrops,toothpaste,whitening,toothpolish,whiteteeth,white,teeth,whiter,website,privacy,policy',
        'keyphrases' => 'pearl drops, white teeth, tooth whitening, teeth whitening, whiter teeth'
    ],

    'third-party-information-collection' => [
        'title' => 'Pearl Drops Third Party Information',
        'desc'  => 'Read about the Third Party information for the Pearl Drops website.',
        'image' => 'pearl-drops-logo.png',
        'pagetype' => 'website',
        'keywords' => 'pearldrops,toothpaste,whitening,toothpolish,whiteteeth,white,teeth,whiter,third,party,data,information,website',
        'keyphrases' => 'pearl drops, white teeth, tooth whitening, teeth whitening, whiter teeth'
    ]
];
