<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Main\PageController@page')->name('home');
Route::get('/our-vision', 'Main\PageController@page')->name('vision');
Route::get('/our-history', 'Main\PageController@page')->name('history');
Route::get('/our-science', 'Main\PageController@page')->name('science');
Route::get('/our-collection', 'Main\PageController@page')->name('collection');
Route::get('/our-collection/{product}', 'Main\PageController@sub');
// Route::get('/contact-us', 'Main\PageController@contact')->name('contact');
// Route::post('/contact-us', 'Main\PageController@contact_submit');
Route::get('/where-to-buy', 'Main\PageController@page')->name('buy');

Route::get('/cookie-notice', 'Main\PageController@misc')->name('cookie-notice');
Route::get('/privacy-policy', 'Main\PageController@misc')->name('privacy-policy');
Route::get('/third-party-information-collection', 'Main\PageController@misc')->name('third-party');
Route::get('/terms-and-conditions', 'Main\PageController@misc')->name('terms');

Route::get('/mailtest', 'Main\PageController@mailtest')->name('mailtest');
Route::get('/instacheck', 'Main\PageController@instacheck')->name('instacheck');

// Misc Sys Admin type stuff.

Route::get('/sitemap', 'Main\MiscController@sitemap')->name('sitemap');
Route::get('/flush', 'Main\MiscController@flush');

// Auto - CRON type
// Route::get('/instagram', 'Auto\InstagramController@fetch')->name('instagram');
/*
Route::get('/phpinfo', function() {
	//echo phpinfo();
	dd(curl_version());
});
*/